import { TestBed, inject } from '@angular/core/testing';

import { CommonVariableService } from './common-variable.service';

describe('CommonVariableService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommonVariableService]
    });
  });

  it('should be created', inject([CommonVariableService], (service: CommonVariableService) => {
    expect(service).toBeTruthy();
  }));
});
