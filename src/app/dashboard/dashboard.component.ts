import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule
  ]
})
export class DashboardComponent implements OnInit {

  userid;
  pubCount=0;
  penCount=0;
  delCount=0;
  cardCount=0;
  favCount=0;
  username='';
  profile:any = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,

  ){

    this.https.getProfle(sessionStorage.getItem('userid')).subscribe(
      success => {
        if (success['status'] == 200) {
          this.profile = success['data'][0];
          this.username = this.profile.firstname+" "+this.profile.lastname;
        //  if(this.profile.photo != "")
           // this.imgURL = this.https.image_url + this.profile.photo;
          //this.oldpath = this.profile.photo;
          console.log("profile====" + JSON.stringify(this.profile));
        }

      }, error => {
        alert('Network Error! Get Profile Details:' + error.statusText);
      });


    this.https.getalldashboardcounting(sessionStorage.getItem('userid')).subscribe(success => {
      if(success['status'] == 200) {
        this.pubCount = success['data'].pub_count;
        this.penCount = success['data'].pen_count;
        this.delCount = success['data'].del_count;
        this.cardCount = success['data'].card_count;
        this.favCount = success['data'].fav_count;
      }  
     // $("#preloader").hide();
    },error=>{
     // alert('Network Error! Get All Profile: '+error.statusText);
     // $("#preloader").hide();
    });


    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
  } 

  ngOnInit() {
    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/login']);
    }


    if(sessionStorage.getItem('role') == '1')
    {
      this.router.navigate(['/']);
    }

    setTimeout(function(){ $('#preloader').fadeOut(); }, 2000);
  }

}
