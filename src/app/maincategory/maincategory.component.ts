import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import { GetnameService } from '../getname.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
} from "@angular/material";
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-maincategory',
  templateUrl: './maincategory.component.html',
  styleUrls: ['./maincategory.component.scss'],
  providers: [
    HttpService, 
    SeoService, 
    GetnameService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
  ]
})
export class MaincategoryComponent implements OnInit {

  profilesearch_control =new FormControl();
  userid;
  categories;
  profiles;
  filter:any;
  view_url;
  categoriess;
 
  category;
  cities:any = [
    {'id':'0','name':'All'},{'id':'1','name':'Villupuram'},{'id':'2','name':'Tindivanam'},{'id':'3','name':'Panruti'},{'id':'4','name':'Thiruvannamalai'},{'id':'5','name':'Pondy'},{'id':'6','name':'Neyveli'}
  ];
  city = this.cities[0].id;
  
  search = {

    "key": "",
    "city": ""
  }
  send_data;
  letter:any = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  directories:any = [];
  profilesearch:any = '';

  constructor(
    private https: HttpService,
    private router: Router,
    private seoservice: SeoService,
    private _getname:GetnameService,
    private snackBar: MatSnackBar,
  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
  }


  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    this.https.getSubcategory(61).subscribe(success => {
      this.categoriess = success['data'];
      this.categories = [];
      let i = 0;
      this.categoriess.forEach(element => {
        this.categories[i] = element;
        this.categories[i].title = element.subcategory.replace(/ /g, "-");
        this.categories[i].title = this.categories[i].title.replace(/_/g, "-");
        this.categories[i].title = this.categories[i].title.replace(/--/g, "-");        
        this.categories[i].title = this.categories[i].title.replace(/&amp;/g, "and");
        this.categories[i].title = this.categories[i].title.replace(/&/g, "and");
        this.categories[i].title = this.categories[i].title.replace(/---/g, "-");
        this.categories[i].title = this.categories[i].title.replace(/"/g, "").replace(/'/g, "").replace(/\(|\)/g, "");
        i++;
      });
      this.category = this.categories[0].subcategoryid;
    }, error => {
      alert('Network Error! Get Subcategory:' + error.statusText);
    });
    $(".loader").hide();
  }

  findWord(i) {    
    $(".word").removeClass('active');
    $(".activeword-"+i).addClass("active");  
    this.filter = this.letter[i];    
  }

  getkeyval(event) {
    this.search.key = event.target.value;
  }

  profileSearch() {
    let categoryname = '';
    let cityname = '';
    let cityid = this.city;
    let categoryid = this.categories[0].subcategoryid;
    categoryname = this._getname.getcategorytitle(this.category,this.categories);  
    this.router.navigate(['/profiles',categoryname,categoryid,cityid]);
  }

  clearFilter() {
    this.filter = "";
    $(".word").removeClass('active');
  } 

  gotoprofile(id) {
     let location = this.city;
    let profilename = this.getprofileName(id);
    this.https.getCategoryId(id).subscribe(success => {
      if(success['status'] == 200) {
        let catid = success['data'][0].subcategoryid;
        this.router.navigate(['profile',profilename,catid,id,0]);
      }
    }, error => {
      alert('Network Error! Get Category ID:' + error.statusText);
    });
  }

  getprofileName(id) {
    let i=0;
    let location = this.city;
    while(this.directories[i]) {
      if(this.directories[i].directoryid == id) {
        return this.directories[i].dirtitle;
      }
      i++;
    }
  }

  loadDirectoryies() {
    this.https.getallprofile(this.city).subscribe(success => {
      console.log(success['data'],'data');
      // if(success['status'] == 200) {
        this.directories = [];
        this.directories = success['data'];
        let i = 0;
        this.directories.forEach(element => {
        this.directories[i] = element;
        this.directories[i].dirtitle = element.directorytitle.replace(/ /g, "-");
        this.directories[i].dirtitle = this.directories[i].dirtitle.replace(/_/g, "-");
        this.directories[i].dirtitle = this.directories[i].dirtitle.replace(/--/g, "-");        
        this.directories[i].dirtitle = this.directories[i].dirtitle.replace(/&amp;/g, "and");
        this.directories[i].dirtitle = this.directories[i].dirtitle.replace(/&/g, "and");
        this.directories[i].dirtitle = this.directories[i].dirtitle.replace(/---/g, "-");
        this.directories[i].dirtitle = this.directories[i].dirtitle.replace(/"/g, "").replace(/'/g, "").replace(/\(|\)/g, "");
        i++;
      });
      // }  else {
        // this.directories = [];
      // }
    }, error => {
      alert('Network Error! Get All Profile:' + error.statusText);
    });
  }

}
