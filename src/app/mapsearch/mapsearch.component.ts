import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { SeoService } from '../seo.service';
import * as $ from 'jquery';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";

@Component({
  selector: 'app-mapsearch',
  templateUrl: './mapsearch.component.html',
  styleUrls: ['./mapsearch.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule
  ]
})

export class MapsearchComponent implements OnInit {
   
  userid;
  categories;
  profiles = [
      "atm", 
      "bakery", 
      "Arulmigu Sri Angala Parameswari Astrology", 
      "Bakkery",
      "accounting",
"airport",
"amusement_park",
"aquarium",
"art_gallery",

"bank",
"bar",
"beauty_salon",
"bicycle_store",
"book_store",
"bowling_alley",
'bus_station',
"cafe",
"campground",
"car_dealer",
"car_rental",
"car_repair",
"car_wash",
"casino",
"cemetery",
"church",
"city_hall",
"clothing_store",
"convenience_store",
"courthouse",
"dentist",
"department_store",
"doctor",
"electrician",
"electronics_store",
"embassy",
"fire_station",
"florist",
"funeral_home",
"furniture_store",
"gas_station",
"gym",
"hair_care",
"hardware_store",
"hindu_temple",
"home_goods_store",
"hospital",
"insurance_agency",
"jewelry_store",
"laundry",
"lawyer",
"library",
"liquor_store",,
"local_government_office",
"locksmith",
"lodging",
"meal_delivery",
"meal_takeaway",
"mosque",
"movie_rental",
"movie_theater",
"moving_company",
"museum",
"night_club",
"painter",
"park",
"parking",
"pet_store",
"pharmacy",
"physiotherapist",
"plumber",
"police",
"post_office",
"real_estate_agency",
"restaurant",
"roofing_contractor",
"rv_park",
"school",
"shoe_store",
"shopping_mall",
"spa",
"stadium",
"storage",
"store",
"subway_station",
"supermarket",
"synagogue",
"taxi_stand",
"train_station",
"transit_station",
"travel_agency",
"veterinary_care",
"zoo"
    ];

  view_url;
  search = {   
    "cat":""
  }
  

  constructor(
    private https: HttpService,
    private router: Router,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
  }
 

  ngOnInit() {
   
    this.userid = sessionStorage.getItem('userid');
    this.https.getCategory().subscribe(success => {     
      this.categories = success['data'];
    }, error => {
      alert('Network Error! Get Category:' + error.statusText);
    }); 

    setTimeout(function(){ $('.loader').fadeOut(); }, 2000); 
    this.https.getallprofile(0).subscribe(success => {          
    }, error => {
      alert('Network Error! Get All Profile:' + error.statusText);
    });
    $.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyAQhqKsAI_XM6c5w5YOCtN4qKiB2Ehb7EI&v=3&callback=initMap&libraries=places");  
  }
    

  getkeyval(event){
    $("#list_type").val(event.target.value);
  }
}

