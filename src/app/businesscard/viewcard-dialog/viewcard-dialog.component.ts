import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpService } from '../../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-viewcard-dialog',
  templateUrl: './viewcard-dialog.component.html',
  styleUrls: ['./viewcard-dialog.component.scss']
})
export class ViewcardDialogComponent implements OnInit {

  card: any = [];

  constructor(
    private https: HttpService,
    private router: Router,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log("card dta=="+JSON.stringify(data));
    }

  ngOnInit() {

    this.https.getCard(this.data.cardid).subscribe(
      success => {
       // console.log("succs=="+JSON.stringify(success['data'][0]));
        if (success['status'] == 200) {
          this.card = success['data'][0];
        }

      }, error => {
        alert('Network Error! Get Profile Details:' + error.statusText);
      });

  }

}
