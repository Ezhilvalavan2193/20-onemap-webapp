import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewcardDialogComponent } from './viewcard-dialog.component';

describe('ViewcardDialogComponent', () => {
  let component: ViewcardDialogComponent;
  let fixture: ComponentFixture<ViewcardDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewcardDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewcardDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
