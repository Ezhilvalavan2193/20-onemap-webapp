import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../http.service';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';
import { SeoService } from '../../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";

@Component({
  selector: 'app-createcard',
  templateUrl: './createcard.component.html',
  styleUrls: ['./createcard.component.scss']
})
export class CreatecardComponent implements OnInit {

  cardData: any = {};
  directoryid: any;
  imgURL: any;
  public imagelogoPath;
  logoError = false;
  send_data:any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private toastr: ToastrService,
  ) { 
    this.route.params.subscribe((params: Params) => {
      this.directoryid = params['id'];
    });

  }

  ngOnInit() {
    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/login']);
    }

    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);

  }

  previewlogo($event) {
    console.log($event.target.files[0].name);

    if ($event.target.files[0].length === 0)
      return;

    var mimeType = $event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    this.imagelogoPath = $event.target.files[0];
    reader.readAsDataURL($event.target.files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  submit(): void{

    this.send_data = {
      "name" : this.cardData.name,
      "email" : this.cardData.email,
      "phone" : this.cardData.phone,
      "logoPath" : this.imgURL,
      "address" : this.cardData.address,
      "memberid" : sessionStorage.getItem('userid')
    }


    if(this.validate(this.send_data))
    {
      this.https.createCard(this.directoryid, this.send_data).subscribe(success => {
        
        console.log("succ==="+JSON.stringify(success));

        if (success['status'] == 200) {
          this.toastr.success('successfully created!', '', {
            timeOut: 6000
          });

          this.router.navigate(['/view', this.directoryid]);
        }
      }, error => {
        console.log("error==="+JSON.stringify(error));
        alert('Network Error! Create Profile:' + JSON.stringify(error));
      });
    }
  }

  validate(send_data)
  {
    var succ = 1;

    if(send_data.name == undefined || send_data.name == "")
    {
      this.toastr.error('Name is a required field.', '', {
        timeOut: 6000
      });

      succ = 0;
    }

    if (!this.isValid(send_data.email)) {

      this.toastr.error('Please enter valid email.', '', {
        timeOut: 6000
      });

      succ = 0;
    }

    if (send_data.phone == undefined || send_data.phone == "") {

      this.toastr.error('Phone number is a required field.', '', {
        timeOut: 6000
      });

      succ = 0;
    }

    if (this.imgURL == "" || this.imgURL == undefined) {
      succ = 0;
      this.logoError = true;
    }

    if(succ == 0)
       return false;
    else
      return true;
  }

  isValid(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  AcceptNumber(event) {
    if (event.keyCode == 8)
      return true;
    else
      if (!((event.keyCode >= 48) && (event.keyCode <= 57))) return false;
  }



}
