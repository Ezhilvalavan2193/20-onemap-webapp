import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {MatPaginator, MatTableDataSource, MatDialog} from '@angular/material';
import { ViewcardDialogComponent } from './viewcard-dialog/viewcard-dialog.component';


@Component({
  selector: 'app-businesscard',
  templateUrl: './businesscard.component.html',
  styleUrls: ['./businesscard.component.scss']
})
export class BusinesscardComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = [
    'id', 
    'name', 
    'subject',
    'createdAt', 
    'actions', 
  ];
  dataSource : any ;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/login']);
    }

    this.https.getcardlist(sessionStorage.getItem('userid')).subscribe(
      success => {       
        this.dataSource = new MatTableDataSource<Element>(success['data']);
      }, error => {
       // alert('Network Error! Get Profile List:' + error.statusText);
      });

    setTimeout(function(){ $('#preloader').fadeOut(); }, 2000);
  }

  openCard(id): void{
    // alert(id);
    let dialogRef = this.dialog.open(ViewcardDialogComponent, {
      width: '600px',
      data: { cardid: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }


}
