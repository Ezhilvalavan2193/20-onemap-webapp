import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'app-categorylist',
  templateUrl: './categorylist.component.html',
  styleUrls: ['./categorylist.component.scss']
})
export class CategorylistComponent implements OnInit {

  categories: any = [];
  filter:any;
  letter:any = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  

  constructor(
    private https: HttpService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {

    this.https.getallsubcategory(61).subscribe(success => {
      console.log("success==="+JSON.stringify(success));
      if (success['status'] == 200) {
        this.categories = success['data'];
      }
      // $("#preloader").hide();
    }, error => {
      // alert('Network Error! Get All Profile: '+error.statusText);
      // $("#preloader").hide();
    });

    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);
  }

  clearFilter() {
    this.filter = "";
    $(".word").removeClass('active');
  } 

  getName(str: String) {
    return str.replace(' ', '+');
  }

  findWord(i) {    
    $(".word").removeClass('active');
    $(".activeword-"+i).addClass("active");  
    this.filter = this.letter[i];    
  }

}
