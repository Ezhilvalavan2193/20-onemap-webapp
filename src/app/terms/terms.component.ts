import { Component, OnInit } from '@angular/core';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.scss'],
  providers:[SeoService,]
})
export class TermsComponent implements OnInit {
  userid;
  constructor(private seoservice: SeoService,) { 
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
  }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
  }
}
