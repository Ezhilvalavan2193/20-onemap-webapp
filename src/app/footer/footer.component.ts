import { Component, OnInit } from '@angular/core';
import { SeoService } from '../seo.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  providers:[SeoService,]
})
export class FooterComponent implements OnInit {

  constructor(private seoservice: SeoService) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
   }

  ngOnInit() {
  }

}
