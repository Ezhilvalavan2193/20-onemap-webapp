import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import * as _ from 'underscore';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";

@Component({
  selector: 'app-reviewlist',
  templateUrl: './reviewlist.component.html',
  styleUrls: ['./reviewlist.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule
  ]
})
export class ReviewlistComponent implements OnInit {
  userid;
  reviews;
  rid;
  count=0;

  constructor( 
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,

  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
   }

 
  deletereview(id) {
    this.https.deletereview(id).subscribe(
      success => {
        if (success['status'] == 200) {
          this.https.getreviewlist(this.userid).subscribe(
            list => {
              this.reviews = list['data'];
              this.count = this.reviews.length;
            }, error => {
              alert('Network Error! Get Review List:' + error.statusText);
            }
          );
        }
      }, error => {
        alert('Network Error! Delete Review:' + error.statusText);
      });
  }

  ngOnInit() {

    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/login']);
    }

    this.https.getreviewlist(sessionStorage.getItem('userid')).subscribe(
      success => {
        console.log(success);
        this.reviews = success['data'];
        this.count = this.reviews.length;
      }, error => {
        alert('Network Error! Get Review List:' + error.statusText);
      });

    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);
  }

}
