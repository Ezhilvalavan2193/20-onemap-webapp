import { Routes } from "@angular/router";
import { CategoryComponent } from './category/category.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import { ProfileComponent } from './profile/profile.component';
import { DetailsComponent } from './details/details.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from "./login/login.component";
import { BusloginComponent } from './buslogin/buslogin.component';
import { BusregisterComponent } from './busregister/busregister.component';
import { BuslistComponent } from './buslist/buslist.component';
import { BuscreateComponent } from './buscreate/buscreate.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ReviewlistComponent } from './reviewlist/reviewlist.component';
import { MapsearchComponent } from './mapsearch/mapsearch.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermsComponent } from './terms/terms.component';
import { ProfileimagesComponent } from './profileimages/profileimages.component';
import { EditionComponent } from './edition/edition.component';
import { EditionbookComponent } from './editionbook/editionbook.component';
import { MaincategoryComponent } from './maincategory/maincategory.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddbusinessComponent } from './addbusiness/addbusiness.component';
import { ViewdirectoryComponent } from './viewdirectory/viewdirectory.component';
import { EditdirectoryComponent } from './editdirectory/editdirectory.component';
import { SearclistComponent } from './searclist/searclist.component';
import { DirectoryComponent } from './directory/directory.component';
import {MyListingComponent} from './my-listing/my-listing.component';
import { AlllistingComponent } from './my-listing/alllisting/alllisting.component';
import { PendinglistingComponent } from './my-listing/pendinglisting/pendinglisting.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { CreatecardComponent } from './businesscard/createcard/createcard.component';
import { UpdatecardComponent} from './businesscard/updatecard/updatecard.component';
import { DeletelistingComponent } from './my-listing/deletelisting/deletelisting.component';
import { BusinesscardComponent } from './businesscard/businesscard.component';
import { CategorylistComponent } from './categorylist/categorylist.component';

export const Approute:Routes = [
    {
        path:'',
        component: CategoryComponent
    },
    {
        path:'subcategory' ,
        component: SubcategoryComponent
    },
    {
        path:'profiles/:result/:sid/:cid' ,
        component: ProfileComponent
    },
    {
        path:'profile/:result/:sid/:pid/:cid' ,
        component: DetailsComponent
    },
    {
        path:'login' ,
        component: LoginComponent
    },
    {
        path:'register',
        component:RegisterComponent
    },
    {
        path:'businessregister',
        component:BusregisterComponent
    },
    {
        path:'businesslogin',
        component:BusloginComponent
    },
    {
        path:'businesslist',
        component:BuslistComponent
    },
    {
        path:'dashboard',
        component:DashboardComponent
    },
    {
        path:'businesscreate',
        component:BuscreateComponent
    },
    {
        path:'forgotpassword',
        component:ForgotpasswordComponent
    },
    {
        path:'changepassword',
        component:ChangepasswordComponent
    },
    {
        path:'reviewlist',
        component:ReviewlistComponent
    },
    {
        path:'mapsearch' ,
        component: MapsearchComponent
    },
    {
        path:'privacypolicy' ,
        component: PrivacypolicyComponent
    },
    {
        path:'terms' ,
        component: TermsComponent
    },
    {
        path:'profileimages',
        component: ProfileimagesComponent
    },
    // {
    //     path:'**',
    //     component: ErrorpageComponent
    // },
    {
        path:'edition',
        component: EditionComponent
    },
    {
        path:'editionbook',
        component: EditionbookComponent
    },
    {
        path:'maincategory',
        component: MaincategoryComponent
    },
    {
        path:'createposting',
        component: AddbusinessComponent
    },
    {
        path:'view/:id',
        component: ViewdirectoryComponent
    },
    {
        path:'editdirectory/:id',
        component: EditdirectoryComponent
    },
    {
        path:'search-lists/:title/:id/:subid/:location/:latitude/:longitude',
        component: SearclistComponent
    },
    {
        path:'directory/:title/:id',
        component: DirectoryComponent
    },
    {
        path:'my-listings',
        component: MyListingComponent
    },
    {
        path: 'all',
        component: AlllistingComponent
    },
    {
        path: 'pending-listings',
        component: PendinglistingComponent
    },
    {
        path: 'delete-listings',
        component: DeletelistingComponent
    },
    {
        path: 'edit-profile',
        component: EditProfileComponent
    },
    {
        path: 'add-card/:id',
        component: CreatecardComponent
    },
    {
        path: 'edit-card/:id',
        component: UpdatecardComponent
    },
    {
        path: 'card-lists',
        component: BusinesscardComponent
    },
    {
        path: 'categories',
        component: CategorylistComponent
    }

];