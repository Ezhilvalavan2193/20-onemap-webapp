import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class HttpService {
  // api_url =  "http://localhost/directoryapi/api/";
  // image_url = "http://localhost/directoryapi/";
  // api_url = "http://mangalaisai.com/directoryapi/api/";
  // api_url = "http://onemapweb.com/directoryapi/api/";
  image_url = "http://onemapweb.com/directoryapi/";
  api_url = "http://onemapweb.com/directoryapi/api/";
  
  headers = new HttpHeaders();

  constructor(
    private http: HttpClient
  )  {
  this.headers = this.headers.append("Authorization", "Basic " + btoa("admin:onemap@123"));
  this.headers = this.headers.append("Content-Type", "application/x-www-form-urlencoded");
  }
  // constructor(private http: HttpClient) { }

  getCategory() {
    return this.http.get(this.api_url + "category", {headers: this.headers});
  }

  
  getCategoryId(id) {
    return this.http.get(this.api_url + "categoryid/"+id, {headers: this.headers});
  }

  getSubcategory(cid) {
    return this.http.get(this.api_url + "subcategory/" + cid, {headers: this.headers});
  }

  getallprofile(id) {
    return this.http.get(this.api_url + "getprofile/"+id, {headers: this.headers});
  }

  getalldirectory() {
    return this.http.get(this.api_url + "getalldirectories", {headers: this.headers});
  }

  getalluser(id) {
    return this.http.get(this.api_url + "getallusers/"+id, {headers: this.headers});
  }

  getallcategory() {
    return this.http.get(this.api_url + "getallcategories", {headers: this.headers});
  }

  getallsubcategory(id) {
    return this.http.get(this.api_url + "subcategory/"+id, {headers: this.headers});
  }

  getallcategoryhome() {
    return this.http.get(this.api_url + "getallcategorieshome", {headers: this.headers});
  }

  getallcounting() {
    return this.http.get(this.api_url + "getallcounting", {headers: this.headers});
  }

  getalldashboardcounting(id) {
    return this.http.get(this.api_url + "getalldashboardcounting/"+id, {headers: this.headers});
  }

  search(senddata) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');    

    return this.http.post(this.api_url + 'search', senddata, {headers: this.headers});
  }

  getallprofilelist(senddata) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');    

    return this.http.post(this.api_url + 'profilesearchfilter', senddata, {headers: this.headers});
  }

  EditProfile(senddata, pid) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');    

    return this.http.post(this.api_url + 'updateprofile/' + pid, senddata, {headers: this.headers});
  }

  getCategorybyid(cid) {
    return this.http.get(this.api_url + "categoryname/" + cid, {headers: this.headers});
  }

  createProfile(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + 'createprofile/' + sessionStorage.getItem('userid'), send_data, {headers: this.headers});
  }

  createDirectory(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    return this.http.post(this.api_url + 'createdirectory', send_data, {headers: this.headers});
  }

  sendcard(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    return this.http.post(this.api_url + 'sendcard', send_data, {headers: this.headers});
  }

  submitreview(userid, send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    return this.http.post(this.api_url + 'submitreview/'+userid, send_data, {headers: this.headers});
  }

  updateDirectory(profileid, send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    return this.http.post(this.api_url + 'updatedirectory/'+profileid, send_data, {headers: this.headers});
  }

  updateCard(profileid, send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    return this.http.post(this.api_url + 'updatecard/'+profileid, send_data, {headers: this.headers});
  }

  createCard(profileid, send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    return this.http.post(this.api_url + 'createcard/'+profileid, send_data, {headers: this.headers});
  }

  updateProfile(userid, send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    
    return this.http.post(this.api_url + 'updateprofile/'+userid, send_data, {headers: this.headers});
  }

  getItemRating(profileid) { 
    return this.http.get(this.api_url + 'getitemrating/' + profileid, {headers: this.headers});
  }

  getAllItemRating(profileid) { 
    return this.http.get(this.api_url + 'getallreviewrating/' + profileid, {headers: this.headers});
  }

  getAllCards(profileid) { 
    return this.http.get(this.api_url + 'getallbusinesscards/' + profileid, {headers: this.headers});
  }

  getAllDirectoryList(id) { 
    return this.http.get(this.api_url + 'getalldirectorylist/' + id, {headers: this.headers});
  }

  getAllPendingDirectoryList(id) { 
    return this.http.get(this.api_url + 'getallpendingdirectorylist/' + id, {headers: this.headers});
  }

  getAllDeletedDirectoryList(id) { 
    return this.http.get(this.api_url + 'getalldeleteddirectorylist/' + id, {headers: this.headers});
  }

  imageupload(pid , send_data){
    return this.http.post(this.api_url + 'imageupload/' + pid, send_data, {headers: this.headers});
  }

  enquiry(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    

    return this.http.post(this.api_url + 'enquiry', send_data, {headers: this.headers});
  }


  getprofilelist(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');    

    return this.http.post(this.api_url + 'viewprofile/' + sessionStorage.getItem('userid'), send_data, {headers: this.headers});
  }

  getcardlist(id) {
    return this.http.get(this.api_url + 'getcardlist/' + id, {headers: this.headers});
  }

  getprofile(rid) {
    return this.http.get(this.api_url + "profilelist/" + rid, {headers: this.headers});
  }


  getreviewlist(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');    

    return this.http.post(this.api_url + 'reviewlist/' + sessionStorage.getItem('userid'), send_data, {headers: this.headers});
  }

  deleteprofile(rid) {
    return this.http.get(this.api_url + "deleteprofile/" + rid, {headers: this.headers});
  }

  deleteDirectory(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + "deletedirectory/" + id, {headers: this.headers});
  }

  deletereview(did) {
    return this.http.get(this.api_url + "deletereview/" + did, {headers: this.headers});
  }

  fetchprofile(id) {
    return this.http.get(this.api_url + "fetchprofile/" + id, {headers: this.headers});
  }

  createLogin(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + 'login', send_data, {headers: this.headers});
  }


  createuser(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + 'register', send_data, {headers: this.headers});
  }

  createbusregister(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + 'register', send_data, {headers: this.headers});
  }


  getSubcategorybyid(sid) {
    return this.http.get(this.api_url + "subcategoryname/" + sid, {headers: this.headers});
  }

  getDirectory(pid, uid) {
    console.log("url"+this.api_url + "getdirectory/id/" + pid+"/userid/"+uid, {headers: this.headers});
    return this.http.get(this.api_url + "getdirectory/id/" + pid+"/userid/"+uid, {headers: this.headers});
  }

  getCard(id) {
    return this.http.get(this.api_url + "getcard/" + id, {headers: this.headers});
  }

  getProfle(id) {
    return this.http.get(this.api_url + "getprofile/" + id, {headers: this.headers});
  }

  downloadPDF(id) {
    return this.http.get(this.api_url + "downloadpdf/" + id, {headers: this.headers});
  }

  getprofiledetails(pid) {
    return this.http.get(this.api_url + "profiledesc/" + pid, {headers: this.headers});
  }
  delelteimage(pid) {
    return this.http.get(this.api_url + "deletephoto/" + pid, {headers: this.headers});
  }

  getimages(pid){
    return this.http.get(this.api_url + "getimages/" + pid, {headers: this.headers});
  }

  updatebanner(id,did){
    return this.http.get(this.api_url + "updatebanner/imgid/"+ id +"/dirid/"+ did, {headers: this.headers});
  }

  getreview(pid) {
    return this.http.get(this.api_url + "viewreview/" + pid, {headers: this.headers});
  }

  setreview(senddata) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + "review", senddata, {headers: this.headers});
  }

  sendnewreview(senddata) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + "newreview", senddata, {headers: this.headers});
  }

  forgotpassword(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + 'forgotpassword', send_data, {headers: this.headers});
  }

  addfavourite(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + 'addfavourite', send_data, {headers: this.headers});
  }


  checkotp(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + 'checkotp', send_data, {headers: this.headers});
  }

  savepassword(send_data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.post(this.api_url + 'resetpassword', send_data, {headers: this.headers});
  }

  overallrate(id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    return this.http.get(this.api_url + 'percentreview/'+id, {headers: this.headers});
  }

}

