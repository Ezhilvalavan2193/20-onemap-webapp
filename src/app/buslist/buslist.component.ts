import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";

@Component({
  selector: 'app-buslist',
  templateUrl: './buslist.component.html',
  styleUrls: ['./buslist.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule
  ]
})

export class BuslistComponent implements OnInit {
  profiles;
  userid;
  edit_id;
  did;
  displayedColumns: string[] = [
    'directoryid', 
    'category', 
    'subcategory', 
    'directorytitle', 
    'directoryaddress', 
    'directorycontactname',
    'images',
    'edit',
    'delete'
  ];
  dataSource : any ;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,

  ){

    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
  } 

  deletedirectory(id) {
    this.https.deleteprofile(id).subscribe(
      success => {
        if (success['status'] == 200) {
          this.https.getprofilelist(this.userid).subscribe(
            list => {
              this.profiles = list['data'];
              console.log(this.profiles)
            }, error => {
              alert('Network Error! Get Profile List:' + error.statusText);
            });
        }
        this.ngOnInit();
      }, error => {
        alert('Network Error! Delete Profile:' + error.statusText);
      });
  }

  Editprofile(id) {
    console.log(id)
    this.router.navigate(['businesscreate',{'pid':id}]);
  }

  imamgedirectory(id){
    this.router.navigate(['profileimages',{'pid':id}]);
  }
  
  ngOnInit() {
    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/businesslogin']);
    }

    this.route.params.subscribe((params: Params) => {
      this.userid = params['cid'];
      this.https.getprofilelist(this.userid).subscribe(
        success => {       
          this.profiles = success['data'];
          this.dataSource = this.profiles;
        }, error => {
          alert('Network Error! Get Profile List:' + error.statusText);
        });
    });
    setTimeout(function(){ $('.loader').fadeOut(); }, 2000);
  }
}