import { ActivatedRoute, Params} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { SeoService } from '../seo.service';
import * as $ from 'jquery';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";

@Component({
  selector: 'app-profilesidebar',
  templateUrl: './profilesidebar.component.html',
  styleUrls: ['./profilesidebar.component.scss'],
  providers: [
    HttpService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule
  ]
})
export class ProfilesidebarComponent implements OnInit {
  view_data;
  category;
  subcategories=new Array();
  subcategory
  cid; 
  
constructor(
  private activatedRoute: ActivatedRoute,
  private https: HttpService,
  private seoservice: SeoService,
  private snackBar: MatSnackBar,
) { 
  this.seoservice.addMeta('description','');
  this.seoservice.addMeta('keyword','');
}


OpenMenu() {
  $(".cat-list").toggleClass('open');
  $(".cat-list").toggle();
}

ngOnInit() {
  this.activatedRoute.params.subscribe((params: Params) => {
    this.cid = params['cid'];
    this.cid = 61;
    this.https.getSubcategory(this.cid).subscribe(
      success => {
        console.log(success);
        this.subcategories = success['data'];
      }, error => {
        alert('Network Error! Get Subcategory:' + error.statusText);
      });

      this.https.getCategorybyid(this.cid).subscribe(success => {
        this.category = success['data'][0];
      }, error => {
        alert('Network Error! Get Category by ID:' + error.statusText);
      })
  });

}

}
