import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit , Inject} from '@angular/core';
import { PagerService } from '../pager.service';
import { HttpService } from '../http.service';
import 'rxjs/Rx';
import * as $ from 'jquery';
// declare var $ : any;
import { SeoService } from '../seo.service';
import { GetnameService } from '../getname.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatDialogModule,
  MatDialog, 
  MatDialogRef, 
  MAT_DIALOG_DATA,

} from "@angular/material";
export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-userdetailspopup',
  templateUrl: './userdetailspopup.component.html',
  styleUrls: ['./userdetailspopup.component.scss'],
  providers: [
    HttpService,
    PagerService,
    SeoService,
    GetnameService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatDialog,
  ]
})
export class UserdetailspopupComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UserdetailspopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private https: HttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private pagerService: PagerService,
    private seoservice: SeoService,
    private _getname: GetnameService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,) {
      console.log(data);
     }

  ngOnInit() {
  }

}
