import { TestBed, inject } from '@angular/core/testing';

import { GetnameService } from './getname.service';

describe('GetnameService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetnameService]
    });
  });

  it('should be created', inject([GetnameService], (service: GetnameService) => {
    expect(service).toBeTruthy();
  }));
});
