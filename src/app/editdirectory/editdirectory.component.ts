import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import * as $ from 'jquery';
import PlaceResult = google.maps.places.PlaceResult;
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { SeoService } from '../seo.service';
import { ToastrService } from 'ngx-toastr';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-editdirectory',
  templateUrl: './editdirectory.component.html',
  styleUrls: ['./editdirectory.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatGoogleMapsAutocompleteModule
  ]
})
export class EditdirectoryComponent implements OnInit {

  business: any = {};
  profileid: any;
  directory: any;
  imgURL: any;
  imgCoverURL: any;
  userid;
  categories;
  subcategories = new Array();
  public imagelogoPath;
  public imagecoverPath;
  country = '';
  state = '';
  city = '';
  postcode = '';
  streetnumber = '';
  address = '';
  latitude = 0.0;
  longitude = 0.0;
  logoError = false;
  coverError = false;

  send_data;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
    private toastr: ToastrService,
    private _flashMessagesService: FlashMessagesService

  ) {
    //this.business.title = "hai";

    this.route.params.subscribe((params: Params) => {
      this.profileid = params['id'];
    });

    this.https.getDirectory(this.profileid, sessionStorage.getItem('userid')).subscribe(
      success => {
        if (success['status'] == 200) {
          this.directory = success['data'][0];
          this.business.title = this.directory.title;
          this.business.tagline = this.directory.tagline;
          this.business.description = this.directory.description;
          this.imgCoverURL = this.https.image_url + this.directory.cover;
          this.imgURL = this.https.image_url + this.directory.logo;
          this.latitude = parseFloat(this.directory.latitude);
          this.longitude = parseFloat(this.directory.longitude);
          this.business.video_url = this.directory.video_url;
          this.business.email = this.directory.email;
          this.business.phone = this.directory.phone;
          this.business.website = this.directory.website;
          this.business.country = this.directory.country;
          this.country = this.directory.country;
          this.business.address = this.directory.address;
          this.address = this.directory.address;
          this.business.state = this.directory.state;
          this.state = this.directory.state;
          this.business.city = this.directory.town;
          this.city = this.directory.town;
          this.business.postcode = this.directory.postcode;
          this.postcode = this.directory.postcode;
          this.streetnumber = this.directory.streetnumber;
          this.business.categoryid = this.directory.categoryid;
          this.business.subcategoryid = this.directory.subcategoryid;
          this.business.company_twitter = (this.directory.company_twitter ? this.directory.company_twitter : "");
          this.business.company_facebook = (this.directory.company_facebook ? this.directory.company_facebook : "");
          this.business.company_instagram = (this.directory.company_instagram ? this.directory.company_instagram : "");
          //this.firstname = this.directory.firstname;
          console.log("directory====" + JSON.stringify(this.directory));

          this.https.getSubcategory(this.directory.categoryid).subscribe(success => {
            console.log(success);
            this.subcategories = success['data'];
          }, error => {
            alert('Network Error! Get Subcategory:' + error.statusText);
          })

        }

      }, error => {
        alert('Network Error! Get Profile Details:' + error.statusText);
      });


    this.seoservice.addMeta('description', '');
    this.seoservice.addMeta('keyword', '');
  }

  loadSubcategory(cid) {
    this.https.getSubcategory(cid).subscribe(success => {
      console.log(success);
      this.subcategories = success['data'];
    }, error => {
      alert('Network Error! Get Subcategory:' + error.statusText);
    })
  }


  ngOnInit() {
    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/login']);
    }

    if(sessionStorage.getItem('role') == '1')
    {
      this.router.navigate(['/']);
    }
    
    this.userid = sessionStorage.getItem('userid');
    this.https.getCategory().subscribe(
      success => {
        console.log(success);
        this.categories = success['data'];
      }, error => {
        alert('Network Error! Get Category:' + error.statusText);
      });

    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);
  }

  previewlogo($event) {
    console.log($event.target.files[0].name);

    if ($event.target.files[0].length === 0)
      return;

    var mimeType = $event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    this.imagelogoPath = $event.target.files[0];
    reader.readAsDataURL($event.target.files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }

  }

  previewcover($event) {
    console.log($event.target.files[0].name);

    if ($event.target.files[0].length === 0)
      return;

    var mimeType = $event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    this.imagecoverPath = $event.target.files[0];
    reader.readAsDataURL($event.target.files[0]);
    reader.onload = (_event) => {
      this.imgCoverURL = reader.result;
    }

  }

  onAutocompleteSelected(result: PlaceResult) {
    console.log('onAddressSelected: ', result.address_components);
    var components = result.address_components;
    var country = null;
    var city = null;
    var postalCode = null;
    var street_number = null;
    var route = null;
    var locality = null;

    for (var i = 0, component; component = components[i]; i++) {
      console.log(component);
      if (component.types[0] == 'country') {
        country = component['long_name'];
      }
      if (component.types[0] == 'administrative_area_level_1') {
        city = component['long_name'];
      }
      if (component.types[0] == 'postal_code') {
        postalCode = component['short_name'];
      }
      if (component.types[0] == 'street_number') {
        street_number = component['short_name'];
      }
      if (component.types[0] == 'route') {
        route = component['long_name'];
      }
      if (component.types[0] == 'locality') {
        locality = component['short_name'];
      }

    }

    console.log("country==" + country + "==city==" + city + "==postcode===" + postalCode + "===street_number==" + street_number + "==route==" + route + "==locality" + locality);

    this.country = country;
    this.state = city;
    this.postcode = postalCode;
    this.streetnumber = street_number;
    this.address=result.formatted_address;
    this.city = locality;

  }

  onLocationSelected(location: Location) {
    console.log('onLocationSelected: ', location);
    this.latitude = location.latitude;
    this.longitude = location.longitude;
  }

  createbusiness() {
    var flashMessagesService = this._flashMessagesService;

    this.send_data = {
      "memberid": this.userid,
      "title": this.business.title,
      "tagline": this.business.tagline,
      "description": this.business.description,
      "logo": this.imgURL,
      "cover": this.imgCoverURL,
      "email": this.business.email,
      "phone": this.business.phone,
      "website": this.business.website,
      "company_twitter": this.business.company_twitter,
      "company_facebook": this.business.company_facebook,
      "company_instagram": this.business.company_instagram,
      "postcode": this.postcode,
      "streetnumber": this.streetnumber,
      "town": this.city,
      "address": this.address,
      "state": this.state,
      "country": this.country,
      "latitude": this.latitude,
      "longitude": this.longitude,
      "categoryid": this.business.categoryid,
      "subcategoryid": this.business.subcategoryid,
      "video_url": this.business.video_url
    }

    if (this.validate(this.send_data)) {
      this.https.updateDirectory(this.profileid, this.send_data).subscribe(success => {
        if (success['status'] == 200) {

          flashMessagesService.show('Successfully created!', { cssClass: 'alert alert-success', timeout: 6000 });

          this.router.navigate(['/view', success['data'][0].id]);
        }
      }, error => {
        // console.log("error==="+JSON.stringify(error));
        // alert('Network Error! Create Profile:' + JSON.stringify(error));
      });
      console.log("submit dta==" + JSON.stringify(this.send_data));
    }




  }

  validate(field)
  {
     var succ = 1;
     if(field.title == undefined || field.title == "")
     {
       succ = 0;
       this.toastr.error('Please enter a title', '', {
        timeOut: 6000
      });
     }

     if(field.tagline == undefined || field.tagline == "")
     {
       succ = 0;
       this.toastr.error('Please enter a tagline', '', {
        timeOut: 6000
      });
     }

     if(field.description == undefined || field.description == "")
     {
       succ = 0
       this.toastr.error('Description is a required field', '', {
        timeOut: 6000
      });
     }

     if(this.imgURL == "" || this.imgURL == undefined)
     {
       succ = 0;
       this.logoError = true;
     }else{
      this.logoError = false;
     }

     if(this.imgCoverURL == undefined || this.imgCoverURL == "")
     {
       succ = 0;
       this.coverError = true;
     }else{
      this.coverError = false;
     }

     if(!this.isValid(field.email))
     {
       succ = 0;
        this.toastr.error('Please enter valid email', '', {
          timeOut: 6000
        });
     }

     if(this.business.phone == undefined || this.business.phone == "")
     {
       succ = 0;
       this.toastr.error('Please enter phone', '', {
        timeOut: 6000
      });
     }

     if(this.address == undefined || this.address == "")
     {
       succ = 0;
       this.toastr.error('Please enter address', '', {
        timeOut: 6000
      });
     }

     if(this.country == undefined || this.country == "")
     {
       succ = 0;
       this.toastr.error('Please enter country', '', {
        timeOut: 6000
      });
     }

     if(this.state == undefined || this.state == "")
     {
       succ = 0;
       this.toastr.error('Please enter state', '', {
        timeOut: 6000
      });
     }

     if(this.city == undefined || this.city == "")
     {
       succ = 0;
       this.toastr.error('Please enter city', '', {
        timeOut: 6000
      });
     }

     if(this.postcode == undefined || this.postcode == "")
     {
       succ = 0;
       this.toastr.error('Please enter postcode', '', {
        timeOut: 6000
      });
     }

     if(this.business.categoryid == "" || this.business.categoryid == undefined)
     {
       succ = 0;
       this.toastr.error('Please choose category', '', {
        timeOut: 6000
      });
     }

     if(this.business.subcategoryid == "" || this.business.subcategoryid == undefined)
     {
       succ = 0;
       this.toastr.error('Please choose subcategory', '', {
        timeOut: 6000
      });
     }

     if(succ == 0)
     return false;
    else
      return true;

  }

  isValid(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  AcceptNumber(event) {
    if (event.keyCode == 8)
      return true;
    else
      if (!((event.keyCode >= 48) && (event.keyCode <= 57))) return false;
  }


}
