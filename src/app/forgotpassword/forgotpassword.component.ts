import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../http.service';
import { SeoService } from '../seo.service';
import * as $ from 'jquery';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
} from "@angular/material";

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss'],
  providers: [
    HttpService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    SeoService,
  ]
})
export class ForgotpasswordComponent implements OnInit {
  send_data;
  email;
  userid;
  constructor
    (private http: Http,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
    private _location: Location)
    {
      this.seoservice.addMeta('description','');
      this.seoservice.addMeta('keyword','');
    }
  forgot() {
    this.send_data = {
      "email": this.email
    }
    if(!(this.validate(this.send_data))) {
    this.https.forgotpassword(this.send_data).subscribe(success => {
      console.log(success);
      if (success['status'] == 200) {
        this.router.navigate(['/changepassword']);
        sessionStorage.setItem('forgetmail', this.email);

      } else {
        alert(success['message']);
      }
    }, error => {
      alert('Network Error! Forgot Password:' + error.statusText);
    });
     }    
  }


  validateEmail(email){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) 
    {      
        return false;
    }
    return true;
  }

  validate(send_data) {

    if(!this.validateEmail(send_data.email)){
      this.snackBar.open('Invalid Email Address!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      
      // $("#toast").html('Invalid Email Address!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);
      return true;
    }
    return false;
  }
  
  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    setTimeout(function(){ $('.loader').fadeOut(); }, 2000);
  }

}
