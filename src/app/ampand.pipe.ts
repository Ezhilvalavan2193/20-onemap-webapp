import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ampand'
})
export class AmpandPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(value) {
      return value.replace(/&amp;/gi, "&");   
    } 
    return value;
  
  }

}
