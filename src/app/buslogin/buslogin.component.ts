import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params, NavigationExtras } from '@angular/router';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
} from "@angular/material";

@Component({
  selector: 'app-buslogin',
  templateUrl: './buslogin.component.html',
  styleUrls: ['./buslogin.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
  ]
})
export class BusloginComponent implements OnInit {

  send_data;
  buslogin;
  username;
  userid;
  result='';
  pid='';
  sid='';
  cid='';
  glogin=false;
  login = {
    "email": "",
    "password": ""
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
   }

  MIDlogin() {
    this.send_data =
      {
        "bid": "1",
        "rid": "0",
        "email": this.login.email,
        "password": this.login.password
      }
    if (!this.validate(this.send_data)) {
      this.https.createLogin(this.send_data).subscribe(success => {
        console.log(success);
        if (success['status'] == 200) {
          sessionStorage.setItem('userid', success['data'][0].memberid);

          if(this.glogin)
          {
            this.router.navigate(['/profile',this.result,this.sid,this.pid,this.cid]);
          }else{
            this.router.navigate(['/businesslist']);
          }

          
        } else if (success['status'] == 504) {
          this.snackBar.open('please check your email and password', '' , {
            verticalPosition: 'top',
            horizontalPosition: 'end',
            duration: 5000,
            panelClass: ['custom-snackbar'],
          });
        }
      }, error => {
        alert('Network Error! Create Login :' + error.statusText);
      });
    }
  }

  validate(send_data) {
    if ((send_data.email == undefined) || (send_data.email == "")) {
      this.snackBar.open('Username will not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        panelClass: ['custom-snackbar'],
        duration: 5000,
      });
      return true;
    }

    if ((send_data.password == undefined) || (send_data.password == "")) {
      this.snackBar.open('Password will not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        panelClass: ['custom-snackbar'],
        duration: 5000,
      });

      return true;
    }
    if ((send_data.bid == undefined) || (send_data.bid == "")) {
      this.snackBar.open('Internal server error!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'left',
        panelClass: ['custom-snackbar'],
        duration: 5000,
      });

      return true;
    }

    if ((send_data.rid == undefined) || (send_data.rid == "")) {
      this.snackBar.open('Internal server error!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        panelClass: ['custom-snackbar'],
        duration: 5000,
      });

      return true;
    }
    return false;
  }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    setTimeout(function(){ $('.loader').fadeOut(); }, 2000);
    
    this.activatedRoute.queryParams.subscribe(params => {
      console.log("gusest===="+JSON.stringify(params));
      this.result = params['result'];
      this.sid = params['sid'];
      this.pid = params['pid'];
      this.cid = params['cid'];
      this.glogin = params['glogin'];
    });


  }

}

