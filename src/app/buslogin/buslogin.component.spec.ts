import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusloginComponent } from './buslogin.component';

describe('BusloginComponent', () => {
  let component: BusloginComponent;
  let fixture: ComponentFixture<BusloginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusloginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusloginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
