import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';
import { FlashMessagesService } from 'angular2-flash-messages';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  profile: any = {};
  imgURL: any;
  logoError = false;
  send_data:any;
  oldpath: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private toastr: ToastrService,
    private _flashMessagesService: FlashMessagesService
  ) {

    this.https.getProfle(sessionStorage.getItem('userid')).subscribe(
      success => {
        if (success['status'] == 200) {
          this.profile = success['data'][0];
          if(this.profile.photo != "")
            this.imgURL = this.https.image_url + this.profile.photo;
          this.oldpath = this.profile.photo;
          console.log("profile====" + JSON.stringify(this.profile));
        }

      }, error => {
        alert('Network Error! Get Profile Details:' + error.statusText);
      });

   }

  ngOnInit() {
    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/login']);
    }
    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);
  }

  previewlogo($event) {
    console.log($event.target.files[0].name);

    if ($event.target.files[0].length === 0)
      return;

    var mimeType = $event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    //this.imagelogoPath = $event.target.files[0];
    reader.readAsDataURL($event.target.files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
      this.oldpath = 'new';
    }

  }


  updateprofile(): void{

    var flashMessagesService = this._flashMessagesService;

    this.send_data = {
      "firstname" : this.profile.firstname,
      "lastname" : this.profile.lastname,
      "email" : this.profile.email,
      "telephone" : this.profile.telephone,
      "photo" : this.imgURL,
      "address" : this.profile.address,
      "oldpath" : this.oldpath
    }

    if(this.validate(this.send_data))
    {
      this.https.updateProfile(sessionStorage.getItem('userid'), this.send_data).subscribe(success => {
        
        console.log("succ==="+JSON.stringify(success));

        if (success['status'] == 200) {
          this.toastr.success('successfully updated!', '', {
            timeOut: 6000
          });
         // flashMessagesService.show('Successfully updated!', { cssClass: 'alert alert-success', timeout: 6000 });
        }
      }, error => {
        console.log("error==="+JSON.stringify(error));
        alert('Network Error! Create Profile:' + JSON.stringify(error));
      });
    }
  }

  validate(send_data)
  {
    var succ = 1;

    if(send_data.firstname == undefined || send_data.firstname == "")
    {
      this.toastr.error('First name is a required field.', '', {
        timeOut: 6000
      });

      succ = 0;
    }

    if(send_data.lastname == undefined || send_data.lastname == "")
    {
      this.toastr.error('Last name is a required field.', '', {
        timeOut: 6000
      });

      succ = 0;
    }

    if (!this.isValid(send_data.email)) {

      this.toastr.error('Please enter valid email.', '', {
        timeOut: 6000
      });

      succ = 0;
    }

    if(send_data.telephone == undefined || send_data.telephone == "")
    {
      this.toastr.error('Phone number is a required field.', '', {
        timeOut: 6000
      });

      succ = 0;
    }

    if (this.imgURL == "" || this.imgURL == undefined) {
      succ = 0;
      this.logoError = true;
    }

    if(succ == 0)
       return false;
    else
      return true;

  }

  isValid(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  AcceptNumber(event) {
    if (event.keyCode == 8)
      return true;
    else
      if (!((event.keyCode >= 48) && (event.keyCode <= 57))) return false;
  }


}
