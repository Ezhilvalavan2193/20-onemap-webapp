
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BarRatingModule } from "ngx-bar-rating";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlashMessagesModule } from 'angular2-flash-messages';
import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule,
  MatDialogModule,
  MatBadgeModule,
  MatExpansionModule,
  MatSliderModule,
  MatSlideToggleModule
} from "@angular/material";
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CategoryComponent } from './category/category.component';
import { SubcategoryComponent } from './subcategory/subcategory.component';
import { ProfileComponent } from './profile/profile.component';
import { ProfilesidebarComponent } from './profilesidebar/profilesidebar.component';
import { DetailsComponent, DialogOverviewExampleDialog } from './details/details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Approute } from "./app.route";
import { RouterModule } from "@angular/router";
import { EnquiryComponent } from './enquiry/enquiry.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { BusloginComponent } from './buslogin/buslogin.component';
import { BusregisterComponent } from './busregister/busregister.component';
import { BuslistComponent } from './buslist/buslist.component';
import { BuscreateComponent } from './buscreate/buscreate.component';
import { AmpandPipe } from './ampand.pipe';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ReviewlistComponent } from './reviewlist/reviewlist.component';

import { MapsearchComponent } from './mapsearch/mapsearch.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { TermsComponent } from './terms/terms.component';
import { AgmCoreModule } from '@agm/core';
import { SubcategoryPipe } from './subcategory.pipe';
import { ProfilePipe } from './profile.pipe';
import { HttpClientModule } from '@angular/common/http';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Ng2OrderModule } from 'ng2-order-pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { ProfileimagesComponent } from './profileimages/profileimages.component';
import { ErrorpageComponent } from './errorpage/errorpage.component';
import { SeoService } from './seo.service';
import { HttpService } from './http.service';
import { EditionComponent } from './edition/edition.component';
import { EditionbookComponent } from './editionbook/editionbook.component';
import { UserdetailspopupComponent } from './userdetailspopup/userdetailspopup.component';
import { MaincategoryComponent } from './maincategory/maincategory.component';
import { ResmenuComponent } from './resmenu/resmenu.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddbusinessComponent } from './addbusiness/addbusiness.component';
import { ViewdirectoryComponent } from './viewdirectory/viewdirectory.component';
import { EditdirectoryComponent } from './editdirectory/editdirectory.component';
import { SearclistComponent } from './searclist/searclist.component';
import {StylePaginatorDirective} from './style-paginator.directive';
import { DirectoryComponent } from './directory/directory.component';
import { LoginDialogComponent } from './directory/login-dialog/login-dialog.component';
import { RegisterDialogComponent } from './directory/register-dialog/register-dialog.component';
import { ReviewDialogComponent } from './directory/review-dialog/review-dialog.component';
import { MyListingComponent } from './my-listing/my-listing.component';
import { MatTabsModule } from '@angular/material';
import { AlllistingComponent } from './my-listing/alllisting/alllisting.component';
import { PendinglistingComponent } from './my-listing/pendinglisting/pendinglisting.component';
import { ToastrModule } from 'ngx-toastr';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { GeocodeService } from './geocode.service';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';
import { BusinesscardComponent } from './businesscard/businesscard.component';
import { CreatecardComponent } from './businesscard/createcard/createcard.component';
import { UpdatecardComponent } from './businesscard/updatecard/updatecard.component';
import { DeletelistingComponent } from './my-listing/deletelisting/deletelisting.component';
import { ShareButtonDialogComponent } from './viewdirectory/share-button-dialog/share-button-dialog.component';
import { SendUserDialogComponent } from './viewdirectory/send-user-dialog/send-user-dialog.component';
import { ViewcardDialogComponent } from './businesscard/viewcard-dialog/viewcard-dialog.component';
import { FavouritelistingComponent } from './my-listing/favouritelisting/favouritelisting.component';
import { CategorylistComponent } from './categorylist/categorylist.component';
import { ForgotDialogComponent } from './login/forgot-dialog/forgot-dialog.component';
import { ResetPasswordDialogComponent } from './login/reset-password-dialog/reset-password-dialog.component';
import { MatProgressBarModule } from '@angular/material';
import { IsLoadingModule } from '@service-work/is-loading';
const googleMapsParams = {
  apiKey: 'AIzaSyAd4jLN8LdoltMtZJfGS3C8ZTgPL7LDtdw',
  libraries: ['places'],
  language: 'en',
  // region: 'DE'
};


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CategoryComponent,
    SubcategoryComponent,
    ProfileComponent,
    ProfilesidebarComponent,
    DetailsComponent,
    EnquiryComponent,
    LoginComponent,
    RegisterComponent,
    BusloginComponent,
    BusregisterComponent,
    BuslistComponent,
    BuscreateComponent,
    AmpandPipe,
    ForgotpasswordComponent,
    ChangepasswordComponent,
    ReviewlistComponent,
    AppComponent,
    MapsearchComponent,
    PrivacypolicyComponent,
    TermsComponent,
    SubcategoryPipe,
    ProfilePipe,
    ProfileimagesComponent,
    ErrorpageComponent,
    EditionComponent,
    EditionbookComponent,
    UserdetailspopupComponent,
    MaincategoryComponent,
    ResmenuComponent,
    DialogOverviewExampleDialog,
    DashboardComponent,
    AddbusinessComponent,
    ViewdirectoryComponent,
    EditdirectoryComponent,
    SearclistComponent,
    StylePaginatorDirective,
    DirectoryComponent,
    LoginDialogComponent,
    RegisterDialogComponent,
    ReviewDialogComponent,
    MyListingComponent,
    AlllistingComponent,
    PendinglistingComponent,
    EditProfileComponent,
    BusinesscardComponent,
    CreatecardComponent,
    UpdatecardComponent,
    DeletelistingComponent,
    ShareButtonDialogComponent,
    SendUserDialogComponent,
    ViewcardDialogComponent,
    FavouritelistingComponent,
    CategorylistComponent,
    ForgotDialogComponent,
    ResetPasswordDialogComponent,
  ],

  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    NgxPaginationModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(Approute, { useHash: true }),
    BarRatingModule,
    FlashMessagesModule.forRoot(),
    ToastrModule.forRoot(),
    MatTabsModule,
    // AgmCoreModule.forRoot({
    //   apiKey: 'AIzaSyBVEOB-ud9gl1FWRmfzwNM9ju2vR9jh2mY'
    // }),
    AgmCoreModule.forRoot(googleMapsParams),
    MatGoogleMapsAutocompleteModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatDialogModule,
    MatBadgeModule,
    MatExpansionModule,
    NgxUsefulSwiperModule,
    MatSliderModule,
    MatSlideToggleModule,
    IsLoadingModule, 
    MatProgressBarModule
  ],
  providers: [
    SeoService,
    HttpService,
    GeocodeService
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogOverviewExampleDialog, LoginDialogComponent, RegisterDialogComponent, ReviewDialogComponent, ShareButtonDialogComponent, SendUserDialogComponent, ViewcardDialogComponent, ForgotDialogComponent, ResetPasswordDialogComponent]
})
export class AppModule { }
