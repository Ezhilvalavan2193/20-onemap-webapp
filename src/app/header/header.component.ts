import { Component, OnInit, Input } from '@angular/core';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import { Router} from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers:[SeoService,]
})

export class HeaderComponent implements OnInit {
  @Input() login: string;
  role: any = 0;
  homecls:any = 'active';
  logincls:any = '';
  businesscls:any = '';
  editioncls:any = '';
  categorycls:any = '';

  constructor(
    private seoservice: SeoService,
    private router: Router
  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
  }

  Logout() {
    sessionStorage.clear();
    this.ngOnInit();
    this.router.navigate(['']);
  }

  OpenMenu() {
    $(".menu-blk").toggleClass('open');
    $(".res-cls").show();
  }
  
  CloseMenu() {
    $(".menu-blk").removeClass('open');
    $(".res-cls").hide();
  }

  ngOnInit() {
    this.login = sessionStorage.getItem('userid');
    this.role = sessionStorage.getItem('role');
  }

  OpenRouter(val) {
    switch(val) {
      case '':
        this.homecls = 'active';
        this.logincls = '';
        this.businesscls = '';
        this.editioncls = '';
        this.categorycls = '';
        this.router.navigate(['/']);
        break;
      case 'login':
        this.homecls = '';
        this.logincls = 'active';
        this.businesscls = '';
        this.editioncls = '';
        this.categorycls = '';
        this.router.navigate(['login']);
        break;
        case 'register':
        this.homecls = '';
        this.logincls = '';
        this.businesscls = '';
        this.editioncls = '';
        this.categorycls = '';
        this.router.navigate(['register']);
        break;
      case 'businesslist':
        this.homecls = '';
        this.logincls = '';
        this.businesscls = 'active';
        this.editioncls = '';
        this.categorycls = '';
        this.router.navigate(['businesslist']);
        break;
      case 'edition':
        this.homecls = '';
        this.logincls = '';
        this.businesscls = '';
        this.editioncls = 'active';
        this.categorycls = '';
        this.router.navigate(['edition']);
        break;  
      case 'maincategory':
        this.homecls = '';
        this.logincls = '';
        this.businesscls = '';
        this.editioncls = '';
        this.categorycls = 'active';
        this.router.navigate(['categories']);
        break;  
        case 'createposting':
        this.homecls = '';
        this.logincls = '';
        this.businesscls = '';
        this.editioncls = '';
        this.categorycls = 'active';
        this.router.navigate(['createposting']);
        break;  
        case 'mylisting':
        this.homecls = '';
        this.logincls = '';
        this.businesscls = '';
        this.editioncls = '';
        this.categorycls = 'active';
        this.router.navigate(['my-listings']);
        break;  
        case 'editprofile':
        this.homecls = '';
        this.logincls = '';
        this.businesscls = '';
        this.editioncls = '';
        this.categorycls = 'active';
        this.router.navigate(['edit-profile']);
        break;  
        case 'dashboard':
        this.homecls = '';
        this.logincls = '';
        this.businesscls = '';
        this.editioncls = '';
        this.categorycls = 'active';
        this.router.navigate(['dashboard']);
        break;  
        case 'reviewlisting':
          this.homecls = '';
          this.logincls = '';
          this.businesscls = '';
          this.editioncls = '';
          this.categorycls = 'active';
          this.router.navigate(['reviewlist']);
          break;  
    } 

  }

  menu: boolean = false;
  openMenu():void {
    this.menu = !this.menu;     
  }

  openmenu() {
    $('.search-blk').toggleClass('open');
  }
}
