import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendUserDialogComponent } from './send-user-dialog.component';

describe('SendUserDialogComponent', () => {
  let component: SendUserDialogComponent;
  let fixture: ComponentFixture<SendUserDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendUserDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendUserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
