import { Component, OnInit, Inject } from '@angular/core';
import { HttpService } from '../../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
import { Observable } from 'rxjs/Observable';
import { MatAutocompleteSelectedEvent, MatChipInputEvent } from '@angular/material';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-send-user-dialog',
  templateUrl: './send-user-dialog.component.html',
  styleUrls: ['./send-user-dialog.component.scss']
})
export class SendUserDialogComponent implements OnInit {

  sendData: any = {};
  filteredOptions: Observable<string[]>;
  users: any = [];
  to_userid: any;
  username: any;
  send_data: any;

  myControl: FormControl = new FormControl();

  constructor(
    private https: HttpService,
    private dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log("data==" + JSON.stringify(data));
  }

  ngOnInit() {

    this.https.getalluser(sessionStorage.getItem('userid')).subscribe(success => {
      if (success['status'] == 200) {
        this.users = success['data'];
      }
      // $("#preloader").hide();
    }, error => {
      // alert('Network Error! Get All Profile: '+error.statusText);
      // $("#preloader").hide();
    });

    this.filteredOptions = this.myControl.valueChanges
      .startWith(null)
      .map(usr => usr && typeof usr === 'object' ? usr.name : usr)
      .map(usr => this.filter(usr));

  }


  filter(val) {
    return val ? this.users.filter(s => s.name.toLowerCase().indexOf(val.toLowerCase()) === 0)
      : this.users;
  }

  onUsrSelectionChanged(event: MatAutocompleteSelectedEvent): void {
    this.to_userid = event.option.id;
    this.username = event.option.viewValue;
  }

  displayDr(user): string {
    return user ? user.name.toString() : user;
  }

  sendClick(): void {

    this.send_data = {
      "fromuser_id" : sessionStorage.getItem('userid'),
      "touser_id" : this.to_userid,
      "subject" : this.sendData.subject,
      "card_id" : this.data.cardid,
    }


    if(this.validate(this.send_data))
    {
      this.https.sendcard(this.send_data).subscribe(success => {
        console.log(success);
        if (success['status'] == 200) {        
          this.dialog.closeAll();
          this.toastr.success('Successfully sent!', '', {
            timeOut: 6000
          });
        } else {
          // this.toastr.error('Please select a user!', '', {
          //   timeOut: 6000
          // });
        }
      }, error => {
       // alert('Network Error! Create Business Register:' + error.statusText);
      });
    }

  }

  validate(send_data)
  {
    var succ = 1;

    if(send_data.touser_id == undefined || send_data.touser_id == "")
    {
      this.toastr.error('Please select a user!', '', {
        timeOut: 6000
      });

      succ = 0;
    }

    if(succ == 0)
      return false;
    else 
      return true;

  }

}
