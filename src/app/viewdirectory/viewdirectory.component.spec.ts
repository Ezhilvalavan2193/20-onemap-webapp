import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewdirectoryComponent } from './viewdirectory.component';

describe('ViewdirectoryComponent', () => {
  let component: ViewdirectoryComponent;
  let fixture: ComponentFixture<ViewdirectoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewdirectoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewdirectoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
