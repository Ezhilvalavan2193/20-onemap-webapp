import { Component, OnInit, Inject } from '@angular/core';
import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../http.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MatDialog } from '@angular/material';
import { SendUserDialogComponent } from '../send-user-dialog/send-user-dialog.component';


@Component({
  selector: 'app-share-button-dialog',
  templateUrl: './share-button-dialog.component.html',
  styleUrls: ['./share-button-dialog.component.scss']
})
export class ShareButtonDialogComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public dialogRef: MatDialogRef<SendUserDialogComponent>,
    private https: HttpService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log("dta==" + JSON.stringify(data));
  }

  ngOnInit() {
  }

  sendToUser(): void {

    this.dialog.closeAll();

    let dialogRef = this.dialog.open(SendUserDialogComponent, {
      width: '600px',
      data: { cardid: this.data.cardid }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
