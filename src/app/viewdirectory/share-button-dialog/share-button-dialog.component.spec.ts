import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareButtonDialogComponent } from './share-button-dialog.component';

describe('ShareButtonDialogComponent', () => {
  let component: ShareButtonDialogComponent;
  let fixture: ComponentFixture<ShareButtonDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareButtonDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareButtonDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
