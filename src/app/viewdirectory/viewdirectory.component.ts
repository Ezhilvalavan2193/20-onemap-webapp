import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import { SwiperOptions } from 'swiper';
import { MatDialog } from '@angular/material';
import { ShareButtonDialogComponent } from './share-button-dialog/share-button-dialog.component';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";
@Component({
  selector: 'app-viewdirectory',
  templateUrl: './viewdirectory.component.html',
  styleUrls: ['./viewdirectory.component.scss']
})


export class ViewdirectoryComponent implements OnInit {

  @ViewChild('videoPlayer') videoplayer: ElementRef;

  cover: any;
  photo: any;
  lat: any;
  lng: any;
  title: any;
  tagline: any;
  description: any;
  video_url: any;
  profileid: any;
  directory: any;
  ratingdata: any;
  twitter_url: any;
  facebook_url: any;
  instagram_url: any;
  firstname: any;
  count = 0;
  fiveStarRating = 0;
  fourStarRating = 0;
  threeStarRating = 0;
  twoStarRating = 0;
  oneStarRating = 0;
  fiveStarRatingPercent = "0%";
  fourStarRatingPercent = "0%";
  threeStarRatingPercent = "0%";
  twoStarRatingPercent = "0%";
  oneStarRatingPercent = "0%";
  averageRating = 0;
  average = 0.0;
  reviewitems: any;
  cards: any;
  cardcount = 0;
  phone:any;
  address:any;
  userid:any;

  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 30
  };


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog

  ) {

    this.userid = sessionStorage.getItem('userid');

    this.route.params.subscribe((params: Params) => {
      this.profileid = params['id'];
    });

    this.https.getDirectory(this.profileid, this.userid).subscribe(
      success => {
        if (success['status'] == 200) {
          this.directory = success['data'][0];
          this.title = this.directory.title;
          this.tagline = this.directory.tagline;
          this.description = this.directory.description;
          if(this.directory.cover)
             this.cover = this.https.image_url + this.directory.cover;
          else 
            this.cover = '../assets/images/cover.jpg';

          if(this.directory.logo)
             this.photo = this.https.image_url + this.directory.logo;
          else 
             this.photo = '../assets/images/profile.jpg';
             
          this.lat = parseFloat(this.directory.latitude);
          this.lng = parseFloat(this.directory.longitude);
          this.video_url = this.directory.video_url;
          this.phone = this.directory.phone;
          this.address = this.directory.postcode + ", " + this.directory.address + ", " + this.directory.town;
          this.twitter_url = (this.directory.company_twitter ? this.directory.company_twitter : "javascript::void(0)");
          this.facebook_url = (this.directory.company_facebook ? this.directory.company_facebook : "javascript::void(0)");
          this.instagram_url = (this.directory.company_instagram ? this.directory.company_instagram : "javascript::void(0)");
          this.firstname = this.directory.firstname;
          console.log("directory====" + JSON.stringify(this.directory));
        }

      }, error => {
        alert('Network Error! Get Profile Details:' + error.statusText);
      });

    this.https.getItemRating(this.profileid).subscribe(success => {

      if (success['status'] == 200) {
        this.ratingdata = success['data'];
        this.count = this.ratingdata.count;
        this.fiveStarRating = this.ratingdata.fiveStarRating;
        this.fourStarRating = this.ratingdata.fourStarRating;
        this.threeStarRating = this.ratingdata.threeStarRating;
        this.twoStarRating = this.ratingdata.twoStarRating;
        this.oneStarRating = this.ratingdata.oneStarRating;
        this.average = this.ratingdata.average;
        this.averageRating = this.ratingdata.averageRating;
        this.fiveStarRatingPercent = this.ratingdata.fiveStarRatingPercent;
        this.fourStarRatingPercent = this.ratingdata.fourStarRatingPercent;
        this.threeStarRatingPercent = this.ratingdata.threeStarRatingPercent;
        this.twoStarRatingPercent = this.ratingdata.twoStarRatingPercent;
        this.oneStarRatingPercent = this.ratingdata.oneStarRatingPercent;
      }


    });

    this.https.getAllItemRating(this.profileid).subscribe(success => {
      if (success['status'] == 200) {
        this.reviewitems = success['data'];
      } else {
        this.reviewitems = [];
      }
    })

    this.https.getAllCards(this.profileid).subscribe(success => {
      if (success['status'] == 200) {
        this.cards = success['data'];
        this.cardcount = this.cards.length;
      } else {
        this.cards = [];
        this.cardcount = 0;
      }
    })

    this.seoservice.addMeta('description', '');
    this.seoservice.addMeta('keyword', '');
  }

  ngOnInit() {
    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/login']);
    }

    if(sessionStorage.getItem('role') == '1')
    {
      this.router.navigate(['/']);
    }

    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);

  }

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

  editClick(id): void {
    this.router.navigate(['/edit-card', id]);
  }

  addClick(id): void {
    this.router.navigate(['/add-card', id]);
  }

  shareClick(id): void{
    let dialogRef = this.dialog.open(ShareButtonDialogComponent, {
      width: '400px',
      data: { cardid: id}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  showPhone(): void{
    $('.phone-hide').addClass('show');
  }


}
