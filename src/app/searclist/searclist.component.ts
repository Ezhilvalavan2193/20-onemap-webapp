import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { HttpService } from '../http.service';
import * as _ from 'underscore';
import { PagerService } from '../pager.service';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import { GetnameService } from '../getname.service';
import PlaceResult = google.maps.places.PlaceResult;
import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';
import { Appearance, GermanAddress, Location } from '@angular-material-extensions/google-maps-autocomplete';
import { 
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatBadgeModule,
  MatExpansionModule,
  MatAutocompleteSelectedEvent,
  MatSliderChange
} from '@angular/material';
// import {startWith} from 'rxjs/operators/startWith';
// import {map} from 'rxjs/operators/map';
import {Observable} from 'rxjs/Observable';
import {FormControl} from '@angular/forms';


@Component({
  selector: 'app-searclist',
  templateUrl: './searclist.component.html',
  styleUrls: ['./searclist.component.scss'],
  providers: [
    HttpService, 
    PagerService, 
    SeoService, 
    GetnameService,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatBadgeModule,
    MatExpansionModule,
    MatGoogleMapsAutocompleteModule,
    MatSliderChange
  ]
})
export class SearclistComponent implements OnInit {

  categoryid:any;
  subcategoryid:any;
  name:any;
  latitude:any;
  longitude:any;
  send_data:any;
  searchresult:any;
  google_map_link:any;
  count: any;
  categories;
  subcategories:any;
  filterData: any = {};
  pagedItems = [];
  pageIndex:number = 0;
  pageSize:number = 9;
  lowValue:number = 0;
  highValue:number = 9; 
  directories:any = [];
  location: any;
  kmvalue = 50;  
  directoryid='';
  directoryname='';
  // filter:any;
  filteredOptions: Observable<string[]>;
  myControl: FormControl = new FormControl();

  constructor(
    private activatedRoute: ActivatedRoute,
    private https: HttpService,
    private pagerService: PagerService,
    private seoservice: SeoService,
    private router: Router,
    private _getname: GetnameService,
  ) {

   
  }

  ngOnInit() {

    // Note: Below 'queryParams' can be replaced with 'params' depending on your requirements
    this.activatedRoute.params.subscribe(params => {
        this.categoryid = params['id'];
        this.subcategoryid = params['subid'];
        this.filterData.categoryid = params['id'];
        this.filterData.subcategoryid = params['subid'];
        this.name = params['title'];
        this.location = params['location'];
        if(params['location'] != 'all')
          this.filterData.location = params['location'];
        this.latitude = params['latitude'];
        this.longitude = params['longitude'];
        console.log("categoryid="+this.categoryid+"=name="+this.name+"=lat="+this.latitude+"=lng="+this.longitude);
    });

    this.https.getalldirectory().subscribe(
      success => {
        console.log("direc=="+JSON.stringify(success));
        this.directories = success['data'];
      }, error => {
        alert('Network Error! Get Category:' + error.statusText);
      });

    this.loadSubcategory(this.categoryid);

  

    this.https.getCategory().subscribe(
      success => {
        console.log(success);
        this.categories = success['data'];
      }, error => {
        alert('Network Error! Get Category:' + error.statusText);
      });

      this.filteredOptions = this.myControl.valueChanges
      .startWith(null)
      .map(dir => dir && typeof dir === 'object' ? dir.name : dir)
      .map(dir => this.filterd(dir));

    this.send_data = {
      "categoryid" : this.categoryid,
      "subcategoryid" : this.subcategoryid,
      "latitude" : this.latitude,
      "longitude" : this.longitude
    }

    this.https.search(this.send_data).subscribe(success => {

      if(success['status'] == 200)
      {
        this.searchresult = success['data'];
        this.google_map_link = "http://maps.google.com/maps?daddr="+this.latitude+"%2C"+this.longitude;
        this.count = this.searchresult.length;
      }else{
        this.searchresult = [];
        this.count = 0;
      }
      
      console.log("search==="+JSON.stringify(success));

    });

    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);

  }

  filterd(val) {
    // alert(JSON.stringify(this.directories));
    // return;
    return val ? this.directories.filter(s => s.name.toLowerCase().indexOf(val.toLowerCase()) === 0)
               : this.directories;
  }


  onDirSelectionChanged(event: MatAutocompleteSelectedEvent): void {
    this.directoryid = event.option.id;
    this.directoryname = event.option.viewValue;
   // ['/directory',hyphenateUrlParams(profile.title),profile.id]
    this.router.navigate(['/directory',this.hyphenateUrlParams(this.directoryname),this.directoryid]);

  }

  displayDr(directory): string {
    return directory ? directory.name.toString() : directory;
 }



  getPaginatorData(event){
    if(event.pageIndex === this.pageIndex + 1){
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue =  this.highValue + this.pageSize;
     }
    else if(event.pageIndex === this.pageIndex - 1){
     this.lowValue = this.lowValue - this.pageSize;
     this.highValue =  this.highValue - this.pageSize;
    }   
   
     this.pageIndex = event.pageIndex;
  }

  searchfilter(): void{

    this.send_data = {
      "categoryid" : 61,
      "subcategoryid" : this.filterData.subcategoryid,
      "title" : this.filterData.title,
      "latitude" : this.latitude,
      "longitude" : this.longitude,
      "radius" : this.kmvalue
    }

    // alert(JSON.stringify(this.send_data));

    this.https.search(this.send_data).subscribe(success => {

      console.log("succcc==="+JSON.stringify(success));

      if(success['status'] == 200)
      {
        this.searchresult = success['data'];
        this.google_map_link = "http://maps.google.com/maps?daddr="+this.latitude+"%2C"+this.longitude;
        this.count = this.searchresult.length;
      }else{
        this.searchresult = [];
        this.count = 0;
      }
    });

  }

  hyphenateUrlParams(str:string){
    return str.replace(' ', '-');
  }


  onAutocompleteSelected(result: PlaceResult) {
    console.log('onAddressSelected: ', result.address_components);
    var components = result.address_components;
    var country = null;
    var city = null;
    var postalCode = null;
    var street_number = null;
    var route = null;
    var locality = null;

    for (var i = 0, component; component = components[i]; i++) {
        console.log(component);
        if (component.types[0] == 'country') {
            country = component['long_name'];
        }
        if (component.types[0] == 'administrative_area_level_1') {
            city = component['long_name'];
        }
        if (component.types[0] == 'postal_code') {
            postalCode = component['short_name'];
        }
        if (component.types[0] == 'street_number') {
            street_number = component['short_name'];
        }
        if (component.types[0] == 'route') {
            route = component['long_name'];
        }
        if (component.types[0] == 'locality') {
            locality = component['short_name'];
        }

    }

    console.log("country=="+country+"==city=="+city+"==postcode==="+postalCode+"===street_number=="+street_number+"==route=="+route+"==locality"+locality);

    // this.country=country;
    // this.state=city;
    // this.postcode=postalCode;
    // this.streetnumber=street_number;
    // this.address=route;
    // this.city=locality;

  }

  loadSubcategory(cid) {
    this.https.getSubcategory(cid).subscribe(success => {
      console.log(success);
      this.subcategories = success['data'];
    }, error => {
      alert('Network Error! Get Subcategory:' + error.statusText);
    })
  }

  onLocationSelected(location: Location) {
    console.log('onLocationSelected: ', location);
    this.latitude = location.latitude;
    this.longitude = location.longitude;
  }

  onSliderChange(event: MatSliderChange) {
    this.kmvalue = event.value;
    console.log("slider val=="+event.value);
  }

  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
  }

}
