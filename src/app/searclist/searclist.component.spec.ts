import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearclistComponent } from './searclist.component';

describe('SearclistComponent', () => {
  let component: SearclistComponent;
  let fixture: ComponentFixture<SearclistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearclistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearclistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
