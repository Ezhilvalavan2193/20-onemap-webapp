import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../http.service';
import $ from 'jquery';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
} from "@angular/material";
import { SeoService } from '../seo.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.scss'],
  providers: [
    HttpService,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    SeoService
  ]
})
export class ChangepasswordComponent implements OnInit {
  send_data;
  userid;
  cpassword = {
    "otp_code": "",
    "password": "",
    "confirmpassword": ""
  }
  constructor(
    private http: Http,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
    private _location: Location) {
      this.seoservice.addMeta('description','');
      this.seoservice.addMeta('keyword','');

  }

  change() {
    if (!this.validate(this.cpassword)) {         
      this.send_data = this.cpassword;
      delete this.send_data.confirmpassword; 
      this.send_data.email = sessionStorage.getItem('forgetmail');
      this.https.checkotp(this.send_data).subscribe(success => {
        console.log(success);
        if(success['status'] == 200){
          sessionStorage.setItem('userid',success['data'].id);
          this.router.navigate(['/']);
        }
      }, error => {
        alert('Network Error! Check OTP:' + error.statusText);
      });
    }
  }

  validate(cpass) {
    let error = 0;
    if(cpass.password !== cpass.confirmpassword){
      this.snackBar.open('Your password is mismatch!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Your password is mismatch!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }
    
    if((cpass.password.length < 5) && (cpass.password.length > 10)){
      this.snackBar.open('Please Enter password length is Min 6 Max 10', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Please Enter password length is Min 6 Max 10');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }


    // alert(cpass.confirmpassword.length);
    if(((cpass.confirmpassword.length) < 5) && ((cpass.confirmpassword.length) > 10)) {
      $("#toast").html('Please Enter Confirm password length is Min 6 Max 10');
      $("#toast").addClass('show');
      setInterval(function () {
        $("#toast").removeClass('show');
      }, 5000);

      return true;
    }
    return false;

  }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    setTimeout(function(){ $('.loader').fadeOut(); }, 2000);
  }
}
