import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule,
  MatDialogModule,
  MatBadgeModule,
  MatExpansionModule
} from "@angular/material";

@Component({
  selector: 'app-edition',
  templateUrl: './edition.component.html',
  styleUrls: ['./edition.component.scss'],
  providers: [
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatDialogModule,
    MatBadgeModule,
    MatExpansionModule,
    HttpService,
    SeoService,
  ]
})
export class EditionComponent implements OnInit {
  userid;
  constructor(
  ) { }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    $(".loader").hide();
  }

  tobook(){
    window.open('http://online.fliphtml5.com/qlulc/vjno/','_blank');
  }

}
