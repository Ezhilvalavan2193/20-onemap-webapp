import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatFormFieldControl,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";

@Component({
  selector: 'app-buscreate',
  templateUrl: './buscreate.component.html',
  styleUrls: ['./buscreate.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule
  ]
})

export class BuscreateComponent implements OnInit {

  view_data;
  categories;
  subcategories = new Array();
  userid;
  pid;
  cid;
  files;
  send_data;
  buscreatemethod:any = {};
  default_image:boolean = true;

  constructor(
    private https: HttpService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
  )  
  {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
  } 
  
  loadSubcategory(cid) {
    this.https.getSubcategory(cid).subscribe(success => {
      console.log(success);
      this.subcategories = success['data'];
    }, error => {
      alert('Network Error! Get Subcategory:' + error.statusText);
    })
  }

  buscreate() {
    
    let img_src = $("#profile-img").attr('src');
    this.send_data = this.buscreatemethod;
    if(img_src.length > 100){
      this.buscreatemethod.imagename = img_src;
    } else {
      this.buscreatemethod.imagename = this.buscreatemethod.imagename;
    }
    delete this.send_data.categoryid;
    if(this.pid){      
      this.https.EditProfile(this.send_data,this.pid).subscribe(success => {
        if (success['status'] == 200) {           
          this.router.navigate(['/businesslist']);
        } 
      }, error => {
        alert('Network Error! Edit Profile:' + error.statusText);
      });
    } else {
      this.https.createProfile(this.send_data).subscribe(success => {
        if (success['status'] == 200) {  
          this.router.navigate(['/businesslist']);
        } 
      }, error => {
        alert('Network Error! Create Profile:' + error.statusText);
      });
    }
  }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    this.activatedRoute.params.subscribe((params: Params) => {
      this.pid = params['pid'];

      if(this.pid) {
        this.https.fetchprofile(this.pid).subscribe(success => {   
          this.buscreatemethod = success['data'][0];
          console.log(this.buscreatemethod,123)
          let img = this.https.image_url+this.buscreatemethod.imagename;
          console.log(img)
          $("#profile-img").attr('src',img);
          this.https.getSubcategory(this.buscreatemethod.categoryid).subscribe(subcategory => {
            this.subcategories = subcategory['data'];
          }, error => {
            alert('Network Error! Get Subcategory:' + error.statusText);
          });
        }, error => {
          alert('Network Error! Fetch Profile:' + error.statusText);
        });
      }
    });

    this.https.getCategory().subscribe(
      success => {
        console.log(success);
        this.categories = success['data'];
        this.cid = this.categories[0].categoryid;
        this.https.getSubcategory(this.cid).subscribe(success => {
          console.log(success);
          this.subcategories = success['data'];
        }, error => {
          alert('Network Error! Get Subcategory:' + error.statusText);
        });
      }, error => {
        alert('Network Error! Get Category:' + error.statusText);
      });
      
      setTimeout(function(){ $('.loader').fadeOut(); }, 2000);
  }

  fileChangeListener($event) {
    console.log($event.target.files[0].name)
    var image: any = new Image();
    var default_image:any = new Image();
    var file: File = $event.target.files[0];     
    var myReader: FileReader = new FileReader();
    var that = this; 
    if($event.target.files[0].name){
      this.default_image = false;
      myReader.onloadend = function (loadEvent: any) {
        image.src = loadEvent.target.result;  
        if(image.src){
          $("#profile-img").attr('src',image.src);
        }else{
          $("#profile-img").attr('src','../../assets/images/default_image.jpg');
        }
      };
      myReader.readAsDataURL(file);
    }else{
      this.default_image = true;
    }
    
  }

  
}
