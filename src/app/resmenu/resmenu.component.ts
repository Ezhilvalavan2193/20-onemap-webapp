import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import * as _ from 'underscore';
import { PagerService } from '../pager.service';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import { GetnameService } from '../getname.service';
import { 
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatBadgeModule,
  MatExpansionModule
} from '@angular/material';

@Component({
  selector: 'app-resmenu',
  templateUrl: './resmenu.component.html',
  styleUrls: ['./resmenu.component.scss'],
  providers: [
    HttpService, 
    PagerService, 
    SeoService, 
    GetnameService,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatBadgeModule,
    MatExpansionModule
  ]
})
export class ResmenuComponent implements OnInit {
  userid;
  homecls:any;
  businesscls:any;
  editioncls:any;
  categoriescls:any;
  constructor( private activatedRoute: ActivatedRoute,
    private https: HttpService,
    private pagerService: PagerService,
    private seoservice: SeoService,
    private router: Router,
    private _getname: GetnameService,) { }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
  }
  
  callPages(item) {
    console.log(item)
    if(item == 'home') {
      this.router.navigate(['/']);
      this.businesscls = "";
      this.editioncls = "";
      this.categoriescls = "";
      this.homecls = 'active';
    } else if(item == 'business'){
      this.businesscls = "active";
      this.editioncls = "";
      this.categoriescls = "";
      this.homecls = '';
      this.router.navigate(['/businesslist']);
    } else if(item == 'category'){
      this.businesscls = "";
      this.editioncls = "";
      this.categoriescls = "active";
      this.homecls = '';
      this.router.navigate(['/maincategory']);
    } else if(item == 'edition'){
      this.businesscls = "";
      this.editioncls = "active";
      this.categoriescls = "";
      this.homecls = '';
      this.router.navigate(['/edition']);
    }
  }
}
