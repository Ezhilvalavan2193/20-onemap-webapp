import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResmenuComponent } from './resmenu.component';

describe('ResmenuComponent', () => {
  let component: ResmenuComponent;
  let fixture: ComponentFixture<ResmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
