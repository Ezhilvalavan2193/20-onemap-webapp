import { ActivatedRoute, Params, NavigationExtras } from '@angular/router';
import { Component, OnInit,Inject } from '@angular/core';
import { PagerService } from '../pager.service';
import { HttpService } from '../http.service';
import { SeoService } from '../seo.service';
import { GetnameService } from '../getname.service';
import * as $ from 'jquery';
import { Router} from '@angular/router';

import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatDialogModule,
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA
} from "@angular/material";

export interface DialogData {
  url: string;
  name: string;
}

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  providers: [
    HttpService,
    PagerService,
    SeoService,
    GetnameService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatDialogModule
  ],
  styles: [`
    .star {
      font-size: 1.5rem;
      color: #b0c4de;
    }
    .filled {
      color: #1e90ff;
    }
    .bad {
      color: #deb0b0;
    }
    .filled.bad {
      color: #ff1e1e;
    }
  `]

})

export class DetailsComponent implements OnInit {
  description_tab='active';
  reviews_tab = '';
  offer_tab = '';
  
  name;
  animal;
  imageslength:any;
  api_url;
  categories;
  ratings:any = {};
  menuactive;
  send_data;
  reviewlength:number;
  reviewform: any = {};
  category = [];
  subcategories;
  subcategory;
  directorytitle;
  subcategorytitle;
  profiles;
  profile: any = {};
  reviews;
  cid = 61;
  sid;
  pager: any = {};;
  pagedItems = [];
  pid;
  images: any;
  img1: any;
  image_array: any = [];
  result;
  cities: any = [
    { 'id': '0', 'name': 'All' }, 
    { 'id': '1', 'name': 'Villupuram' }, 
    { 'id': '2', 'name': 'Tindivanam' }, 
    { 'id': '3', 'name': 'Panruti' }
  ];
  city;
  currentRate;
  collection = [];
  rate = '0';
  userid;
  review;
  image_url;
  
  default_image: boolean = false;
  pageIndex:number = 0;
  pageSize:number = 6;
  lowValue:number = 0;
  highValue:number = 6; 
  
  constructor(
    private https: HttpService,
    private activatedRoute: ActivatedRoute,
    private pagerService: PagerService,
    private seoservice: SeoService,
    public dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar,
  ) {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.city = params['cid'];
      this.sid = params['sid'];
      this.pid = params['pid'];
      this.result = params['result'];
    });
    
    // get profiles
    this.https.getprofiledetails(this.pid).subscribe(
      success => {
        if(success['status'] == 200) {
          this.profile = success['data'][0];
          if (this.profile.image == null || this.profile.image == 'undefined' || this.profile.image == '') {
            this.default_image = true;
          } else {
            this.default_image = false;
          }
          this.seoservice.addMeta('description', this.profile.meta_description);
          this.seoservice.addMeta('keyword', this.profile.keywords);
        } else {
          this.profile = {};
        }
      }, error => {
        alert('Network Error! Get Profile Details:' + error.statusText);
      });
  }

  ngOnInit() {
    // return;
    setTimeout(function () {
      $('.loader').fadeOut();
    }, 2000);
    this.profile.rating = 0.0;
    this.image_url = this.https.image_url;
    this.userid = sessionStorage.getItem('userid');
    this.activatedRoute.params.subscribe((params: Params) => {
      this.city = params['cid'];
      this.sid = params['sid'];
      this.pid = params['pid'];
      this.result = params['result'];
      
      // get subcategory
      if (this.sid !== undefined) {
        this.https.getSubcategorybyid(this.sid).subscribe(
          success => {
            this.subcategory = success['data'][0].subcategory;
            this.subcategorytitle = this.subcategory.replace(/ /g, "-");
            this.subcategorytitle = this.subcategorytitle.replace(/_/g, "-");
            this.subcategorytitle = this.subcategorytitle.replace(/&amp;/g, "and");
            this.subcategorytitle = this.subcategorytitle.replace(/&/g, "and");
            this.subcategorytitle = this.subcategorytitle.replace(/--/g, "-");
            this.subcategorytitle = this.subcategorytitle.replace(/---/g, "-");
            this.subcategorytitle = this.subcategorytitle.replace(/"/g, "").replace(/'/g, "").replace(/\(|\)/g, "");
          }, error => {
            alert('Network Error! Get Subcategory by ID:' + error.statusText);
          });
      }

      // get profiles
      this.https.getprofiledetails(this.pid).subscribe(
        success => {
          console.log(success);
          if(success['status'] == 200) {
            this.profile = success['data'][0];
            if (this.pid) {
              this.directorytitle = this.profile.directorytitle;
            }
          }
        }, error => {
          alert('Network Error! Get Profile Details:' + error.statusText);
        });

      //get images
      this.https.getimages(this.pid).subscribe(
        success => {
          this.images = success['data'];
          this.image_array = [];
          this.images.forEach(element => {
            if (element.banner_image == 1) {
              this.img1 = element.imagename;
            } 
            this.image_array.push(element);
          });
          this.imageslength = this.image_array.length;
        }, error => {
          alert('Network Error! Get Images:' + error.statusText);
        });
      //update images


      // get review
      this.https.getreview(this.pid).subscribe(
        success => {
          this.reviews = success['data'];
          this.reviewlength = this.reviews.length;
        }, error => {
          alert('Network Error! Get Review:' + error.statusText);
        });
    });

    this.https.overallrate(this.pid).subscribe(
      success => {
        this.ratings = success['data']; 
      }, error => {
        alert('Network Error! Get Overall Ratings:' + error.statusText);
      });
    window.scroll(0, 0);
  }

  openDialog(url,imagename): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '600px',
      data: {name: imagename, url: url}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }

  getPaginatorData(event){
    if(event.pageIndex === this.pageIndex + 1){
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue =  this.highValue + this.pageSize;
     }
    else if(event.pageIndex === this.pageIndex - 1){
     this.lowValue = this.lowValue - this.pageSize;
     this.highValue =  this.highValue - this.pageSize;
    }   
   
     this.pageIndex = event.pageIndex;
  }

  // set review
  setReview(val) {

  if(this.userid == null)
  {
    if(confirm("Login to leave a review")) {

      let navigationExtras: NavigationExtras = {
        queryParams: {
            "result": this.result,
            "cid" : this.cid,
            "sid" : this.sid,
            "pid" : this.pid,
            "glogin" : true
        }
      };

      this.router.navigate(['businesslogin'], navigationExtras);
    }
  }
  


    if(val == 'submit') {
      let send_data =   {
        "review": this.review,
        "rating": this.rate
      }
      if(this.validateFirstReview(send_data)) {
        // $(".review").show();
        // $(".review.modal").show();
        // $("#review-register").show();
        // $("#your-review-form").hide();

        this.sendreview();
      }
    } else {
      $(".review").show();
      $(".review.modal").show();
      $("#review-register").hide();
      $("#your-review-form").show();
    }
  }

  closeModal() {
    $(".review").hide();
    $(".review.modal").hide();
    $("#review-register").hide();
    $("#your-review-form").hide();
  }

  sendreview() {
    // $("#review-register").show();
    // $("#your-review-form").hide();
    // $(".review.modal").show();
    this.send_data = {
      "review": this.review,
      "rating": this.rate,
      "directoryid": this.pid,
      "userid" : this.userid
    }
   
    if (this.validateReview(this.send_data)) {
      $(".loader").show();
      this.https.sendnewreview(this.send_data).subscribe(success => {
        if (success['status'] == 200) {
          this.review = "";
          this.closeModal();
          this.ngOnInit();

          this.snackBar.open('Thanks for your feedback!', '' , {
            verticalPosition: 'top',
            horizontalPosition: 'end',
            duration: 5000,
            panelClass: ['custom-snackbar'],
          });
        }
      }, error => {
        alert('Network Error! Send New Review:' + error.statusText);
      });
    }
  }

  //validation for Review Form
  validateReview(send_data) {
    let error = true;
    if ((send_data['review'] == '') || (send_data['review'] == undefined)) {
      alert('please enter review');
      error = false;
    }

    // if ((send_data['email'] == '') || (send_data['email'] == undefined)) {
    //   alert('please enter email');
    //   error = false;
    // }

    // if ((send_data['name'] == '') || (send_data['name'] == undefined)) {
    //   alert('please enter name');
    //   error = false;
    // }

    // if ((send_data['phone'] == '') || (send_data['phone'] == undefined)) {
    //   alert('please enter phone');
    //   error = false;
    // }

    if ((send_data['rating'] == '') || (send_data['rating'] == undefined)) {
      alert('please enter rating');
      error = false;
    }

    return error;
  }

  validateFirstReview(send_data) {
    let error = true;
    if ((send_data['review'] == '') || (send_data['review'] == undefined)) {
      alert('please enter review');
      error = false;
    }

    if ((send_data['rating'] == '') || (send_data['rating'] == undefined)) {
      alert('please enter rating');
      error = false;
    }

    return error;
  }

  setPage(page: number) {

    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.reviews.length, page);

    // get current page of items
    this.pagedItems = this.reviews.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  settabactive(event) {
    if(event == 'description') {
      this.description_tab='active';
      this.reviews_tab = '';
      this.offer_tab = '';
    }

    if(event == 'reviews') {
      this.description_tab='';
      this.reviews_tab = 'active';
      this.offer_tab = '';
    }

    if(event == 'offer') {
      this.description_tab='';
      this.reviews_tab = '';
      this.offer_tab = 'active';
    }
  }
}

@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialog {
  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}