import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import { GetnameService } from '../getname.service';
import {Appearance, GermanAddress, Location} from '@angular-material-extensions/google-maps-autocomplete';
import PlaceResult = google.maps.places.PlaceResult;
import {MatGoogleMapsAutocompleteModule} from '@angular-material-extensions/google-maps-autocomplete';

// import {startWith} from 'rxjs/operators/startWith';
// import {map} from 'rxjs/operators/map';
import {Observable} from 'rxjs/Observable';
import {MatAutocompleteSelectedEvent, MatChipInputEvent} from '@angular/material';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
} from "@angular/material";
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss'],
  providers: [
    HttpService, 
    SeoService, 
    GetnameService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatGoogleMapsAutocompleteModule
  ]
})

export class CategoryComponent implements OnInit {

  //profilesearch_control =new FormControl();
  userid;
  categories;
  profiles;
  country = '';
  state = '';
  city = '';
  postcode = '';
  streetnumber = '';
  fulladdress = 'all';
  address = '';
  latitude = 0.0;
  longitude = 0.0;
  // filter:any;
  filteredOptions: Observable<string[]>;
  filteredPostcode: Observable<string[]>;
  view_url;
  categoriess;
  subcategories:any;
  filterData:any = {};
 
  category;
  cities:any = [
    {'id':'0','name':'All'},
    {'id':'1','name':'Villupuram'},
    {'id':'2','name':'Tindivanam'},
    {'id':'3','name':'Panruti'},
    {'id':'4','name':'Thiruvannamalai'},
    {'id':'5','name':'Pondy'},
    {'id':'6','name':'Neyveli'}
  ];

  cityid = "0";
  cityname ='';
  // city = this.cities[0].id;

  directoryid='';
  directoryname='';
  dir_count=0;
  usr_count=0;
  cat_count=0;
  
  // search = {
  //   "key": "",
  //   "city": ""
  // }

  send_data;
  letter:any = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  directories:any = [];
  profilesearch:any = '';
  categorylist:any = [];

  
  myControl: FormControl = new FormControl();


  constructor(
    private https: HttpService,
    private router: Router,
    private route: ActivatedRoute,
    private seoservice: SeoService,
    private _getname:GetnameService
  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
  }


  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');

    this.filterData.categoryid = "61";
    this.loadSubcategory(this.filterData.categoryid);
    
    this.https.getCategory().subscribe(
      success => {
        console.log(success);
        this.categorylist = success['data'];
      }, error => {
        alert('Network Error! Get Category:' + error.statusText);
      });

        
    this.https.getalldirectory().subscribe(
      success => {
        console.log(success);
        this.directories = success['data'];
      }, error => {
        alert('Network Error! Get Category:' + error.statusText);
      });
   
    this.https.getallcounting().subscribe(success => {

      console.log("count----"+JSON.stringify(success));

      if(success['status'] == 200) {
        this.dir_count = success['data'].dir_count;
        this.usr_count = success['data'].usr_count;
        this.cat_count = success['data'].cat_count;
      }  
     // $("#preloader").hide();
    },error=>{
      console.log("counterror=="+JSON.stringify(error));
     alert('Network Error! Get All Profile: '+error.statusText);
     $("#preloader").hide();
    });

    this.https.getallcategoryhome().subscribe(success => {
      if(success['status'] == 200) {
        this.categories = success['data'];
      }  
      //$("#preloader").hide();
    },error=>{
      //alert('Network Error! Get All Profile: '+error.statusText);
     // $("#preloader").hide();
    });



    this.filteredOptions = this.myControl.valueChanges
    .startWith(null)
    .map(dir => dir && typeof dir === 'object' ? dir.name : dir)
    .map(dir => this.filter(dir));

    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);

  }

  filter(val) {
    return val ? this.directories.filter(s => s.name.toLowerCase().indexOf(val.toLowerCase()) === 0)
               : this.directories;
  }
  

  // filterPostcode(val) {
  //   return val ? this.cities.filter(s => s.name.toLowerCase().indexOf(val.toLowerCase()) === 0)
  //              : this.cities;
  // }

  onDirSelectionChanged(event: MatAutocompleteSelectedEvent): void {
    this.directoryid = event.option.id;
    this.directoryname = event.option.viewValue;
   // ['/directory',hyphenateUrlParams(profile.title),profile.id]
    this.router.navigate(['/directory',this.hyphenateUrlParams(this.directoryname),this.directoryid]);

  }

  onPostSelectionChanged(event: MatAutocompleteSelectedEvent): void {
    this.cityid = event.option.id;
  }
    

  displayDr(directory): string {
      return directory ? directory.name.toString() : directory;
   }

   displayCt(city): string {
    return city ? city.name.toString() : city;
 }

 loadSubcategory(cid) {
  this.https.getSubcategory(cid).subscribe(success => {
    console.log(success);
    this.subcategories = success['data'];
  }, error => {
    alert('Network Error! Get Subcategory:' + error.statusText);
  })
}

 search()
 {

  let subcategoryid = 0;

   let name = "Weddings";
   let id = 61;
   if(this.filterData.subcategoryid != undefined)
      subcategoryid = this.filterData.subcategoryid;

   console.log("id==="+id+"===subid=="+subcategoryid);

  //  return;
   
   if(subcategoryid == 0)
   {
     alert("Please select subcategory");
     return;
   }

   //let searchparams = {id : id, name : name, latitude : this.latitude, longitude : this.longitude};

   //this.router.navigate(['/search-lists/'+name.toLowerCase()], { relativeTo: this.route, queryParams: searchparams, queryParamsHandling: 'preserve'});
   this.router.navigate(['/search-lists',name.toLowerCase(),id,subcategoryid,this.fulladdress,this.latitude, this.longitude]);
 }

  onAutocompleteSelected(result: PlaceResult) {
    console.log('onAddressSelected: ', result.formatted_address);
    var components = result.address_components;
    var country = null;
    var city = null;
    var postalCode = null;
    var street_number = null;
    var route = null;
    var locality = null;
    this.fulladdress = result.formatted_address;

    for (var i = 0, component; component = components[i]; i++) {
        console.log(component);
        if (component.types[0] == 'country') {
            country = component['long_name'];
        }
        if (component.types[0] == 'administrative_area_level_1') {
            city = component['long_name'];
        }
        if (component.types[0] == 'postal_code') {
            postalCode = component['short_name'];
        }
        if (component.types[0] == 'street_number') {
            street_number = component['short_name'];
        }
        if (component.types[0] == 'route') {
            route = component['long_name'];
        }
        if (component.types[0] == 'locality') {
            locality = component['short_name'];
        }

    }

    console.log("country=="+country+"==city=="+city+"==postcode==="+postalCode+"===street_number=="+street_number+"==route=="+route+"==locality"+locality);

    this.country=country;
    this.state=city;
    this.postcode=postalCode;
    this.streetnumber=street_number;
    this.address=route;
    this.city=locality;

  }

  onLocationSelected(location: Location) {
    console.log('onLocationSelected: ', location);
    this.latitude = location.latitude;
    this.longitude = location.longitude;
  }

onBlurMethod(val)
{
  //alert(val);
  if(val != undefined && val != ""){
    this.fulladdress = val;
  }else{
    this.fulladdress = '';
  }
}

getName(str: String)
{
  return str.replace(' ', '+');
}

hyphenateUrlParams(str:string){
  return str.replace(' ', '-');
}
  
}
