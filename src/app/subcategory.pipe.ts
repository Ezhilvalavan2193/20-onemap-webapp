import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'subcategory'
})
export class SubcategoryPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(!args)
      return value;

    return value.filter(item => item.subcategory[0].indexOf(args) !== -1);
  }

}
