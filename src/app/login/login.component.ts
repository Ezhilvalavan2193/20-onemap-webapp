import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { SeoService } from '../seo.service';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { ForgotDialogComponent } from './forgot-dialog/forgot-dialog.component';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [
    HttpService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    SeoService,
  ]
})

export class LoginComponent implements OnInit {
  send_data;
  buslogin;
  username;
  userid;
  result = '';
  pid = '';
  sid = '';
  cid = '';
  glogin = false;
  login = {
    "email": "",
    "password": ""
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private _location: Location,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private toastr: ToastrService,
  ) {
    this.seoservice.addMeta('description', '');
    this.seoservice.addMeta('keyword', '');
  }

  MIDlogin() {
    this.send_data =
    {
      "email": this.login.email,
      "password": this.login.password
    }
    if (this.validate(this.send_data)) {
      this.https.createLogin(this.send_data).subscribe(success => {
        console.log(success);
        if (success['status'] == 200) {
          sessionStorage.setItem('userid', success['data'][0].memberid);
          sessionStorage.setItem('role', success['data'][0].status);
          this.router.navigate(['/']);

        } else if (success['status'] == 504) {
          this.toastr.error('please check your email and password!', '', {
            timeOut: 6000
          });
        }
      }, error => {
        alert('Network Error! Create Login :' + error.statusText);
      });
    }
  }

  validate(send_data) {

    var succ = 1;

    if ((send_data.email == undefined) || (send_data.email == "")) {
      this.toastr.error('Please enter valid email!', '', {
        timeOut: 6000
      });
      succ = 0;
    }

    if ((send_data.password == undefined) || (send_data.password == "")) {

      this.toastr.error('Please enter your password!', '', {
        timeOut: 6000
      });
      succ = 0;
    }

    if (succ == 0)
      return false;
    else
      return true;
  }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    // if (sessionStorage.getItem('redirect') == '2') {
    //   this._location.back();
    // }
    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);

    // this.activatedRoute.queryParams.subscribe(params => {
    //   console.log("gusest===="+JSON.stringify(params));
    //   this.result = params['result'];
    //   this.sid = params['sid'];
    //   this.pid = params['pid'];
    //   this.cid = params['cid'];
    //   this.glogin = params['glogin'];
    // });

  }

  openForgot(): void{
    let dialogRef = this.dialog.open(ForgotDialogComponent, {
      width: '400px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}

