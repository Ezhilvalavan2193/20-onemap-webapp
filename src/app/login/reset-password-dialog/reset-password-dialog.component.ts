import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpService } from '../../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password-dialog',
  templateUrl: './reset-password-dialog.component.html',
  styleUrls: ['./reset-password-dialog.component.scss']
})
export class ResetPasswordDialogComponent implements OnInit {

  reset: any = {};
  send_data:any;
  errorMsg:any;

  constructor(
    private https: HttpService,
    private router: Router,
    public dialog: MatDialog,
    private toastr: ToastrService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log("dta=="+JSON.stringify(data));
     }

  ngOnInit() {
  }

  change(str): void{
    console.log("str="+str);
    if(str.length == 6)
    {

      this.send_data = {
        "email" : this.data.email,
        "otp_code" : str
      }

      this.https.checkotp(this.send_data).subscribe(success => {
        console.log(success);
        if(success['status'] == 200){
          this.errorMsg = '';   
         // $("#password").removeAttr('disabled');
          $("#submit").removeAttr('disabled');
        }else{
          this.errorMsg = 'Please enter valid OTP';
         // $("#password").attr('disabled','disabled');
          $("#submit").attr('disabled','disabled');
        }
      }, error => {
        //alert('Network Error! Check OTP:' + error.statusText);
      });
    }else{
      $("#password").val('');
     // $("#password").attr('disabled','disabled');
      $("#submit").attr('disabled','disabled');
    }
  }

  submitClick(): void{

    this.send_data = {
      "email" : this.data.email,
      "password" : this.reset.password
    }

    this.https.savepassword(this.send_data).subscribe(success => {
      console.log(success);
      if (success['status'] == 200) {
        this.toastr.success('successfully changed', '', {
          timeOut: 6000
        });
        this.dialog.closeAll();
        let currentUrl = this.router.url;
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
            this.router.navigate([currentUrl]);
        });
      } else if (success['status'] == 504) {
        this.toastr.error('Please enter valid OTP', '', {
          timeOut: 6000
        });
      }
    }, error => {
      alert('Network Error! Create Login :' + error.statusText);
    });
  }

}
