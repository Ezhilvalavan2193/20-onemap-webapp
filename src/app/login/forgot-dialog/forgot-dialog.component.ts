import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../../http.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { ResetPasswordDialogComponent } from '../reset-password-dialog/reset-password-dialog.component';

@Component({
  selector: 'app-forgot-dialog',
  templateUrl: './forgot-dialog.component.html',
  styleUrls: ['./forgot-dialog.component.scss']
})
export class ForgotDialogComponent implements OnInit {

  send_data:any;
  errorMsg = '';
  forgot = {
    "email": ""
  }

  constructor(
    private https: HttpService,
    private router: Router,
    private toastr: ToastrService,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  submitClick(): void {
    // this.dialogRef.close();
    this.send_data = {
      "email" : this.forgot.email,
    }

    if(this.validate(this.send_data))
    {
        this.https.forgotpassword(this.send_data).subscribe(success => {
          console.log("success=="+JSON.stringify(success));
          if (success['status'] == 200) {  
            this.errorMsg = '';

            this.toastr.success('check your email for you new password', '', {
              timeOut: 6000
            });

            this.dialog.closeAll();

            this.openReset(this.forgot.email);
           
          }else{
            this.errorMsg = "Please enter your registered email!";
          }
        }, error => {

        });
    // console.log("submit dta=="+JSON.stringify(this.send_data));
    }

  }

  openReset(email): void{
    let dialogRef = this.dialog.open(ResetPasswordDialogComponent, {
      width: '600px',
      data: { email : email}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  validate(send_data)
  {
     var succ = 1;

     if(send_data.email == undefined || send_data.email == "")
     {
      this.toastr.error('Please enter valid email!', '', {
        timeOut: 6000
      });
       succ = 0;
     }

     if(succ == 0)
     return false;
    else
      return true;
  }

}
