import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileimagesComponent } from './profileimages.component';

describe('ProfileimagesComponent', () => {
  let component: ProfileimagesComponent;
  let fixture: ComponentFixture<ProfileimagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileimagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileimagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
