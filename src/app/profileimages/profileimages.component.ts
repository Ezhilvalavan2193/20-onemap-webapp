import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatFormFieldControl,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";

@Component({
  selector: 'app-profileimages',
  templateUrl: './profileimages.component.html',
  styleUrls: ['./profileimages.component.scss'],
  providers:[
    HttpService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule
  ]
})
export class ProfileimagesComponent implements OnInit {
  pid:any;
  images;
  img_url;
  imageCollection:any = {};
  send_data;
  default_image:boolean =false;
  userid = sessionStorage.getItem('userid');
  constructor(private route: ActivatedRoute,
    private router: Router,
    private https: HttpService) { }

  ngOnInit() {
    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/businesslogin']);
    }
    setTimeout(function(){ $('.loader').fadeOut(); }, 2000);
    this.route.params.subscribe((params: Params) => {
      this.pid = params['pid'];
      console.log(this.pid);
    });
    this.img_url = this.https.image_url;
    console.log(this.img_url);
    this.https.getimages(this.pid).subscribe(
      success=>{
        this.images=success['data'];
      }, error => {
        alert('Network Error! Get Images:' + error.statusText);
      }
    );
  }


  fileChangeListener($event) {
    var image: any = new Image();
    var file: File = $event.target.files[0];     
    var myReader: FileReader = new FileReader();
    if($event.target.files[0].name){
      myReader.onloadend = function (loadEvent: any) {
        image.src = loadEvent.target.result;
        $("#profile_img").attr('src',image.src);
      };
      if(image.src){
        this.default_image = false;
      }else{
        this.default_image = true;
      }
      myReader.readAsDataURL(file);
    }else{
      this.default_image = true;
    }
    
  }
 
  saveImage() {
    let img_src = $("#profile_img").attr('src');
    this.send_data = {
      'imagename':  img_src
    }
    console.log(this.send_data);
    this.https.imageupload(this.pid, this.send_data).subscribe(
      success=>{
        console.log(success,'img response');
        if (success['status'] == 200) {   
          console.log(success,'get img');        
          // this.router.navigate(['/businesslist']);
          this.https.getimages(this.pid).subscribe(
          success=>{
            this.images=success['data'];
          }, error => {
            alert('Network Error! Get Images:' + error.statusText);
          }
          );
        } 
      }, error => {
        alert('Network Error! Image Upload:' + error.statusText);
      }
    )
    }

    deleteImage(id) {
      this.https.delelteimage(id).subscribe( success => {
        if (success['status'] == 200) {           
          // this.router.navigate(['/businesslist']);
          this.https.getimages(this.pid).subscribe(
            success=>{
              this.images=success['data'];
            }, error => {
              alert('Network Error! Get Images:' + error.statusText);
            }
          );
        } 
      }, error => {
        alert('Network Error! Delete Image:' + error.statusText);
      });
    }

  updateBanner(id){
    this.https.updatebanner(id, this.pid).subscribe(success=>{

      }, error => {
      alert('Network Error! Update Banner:' + error.statusText);
    });
  }
}
