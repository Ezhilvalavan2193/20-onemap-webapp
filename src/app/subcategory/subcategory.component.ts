import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule
  ]
})
export class SubcategoryComponent implements OnInit {
  view_data;
  category = {
    "category":""
  };
  subcategories;
  cid;
  categories;
  userid;

  constructor(
    private https: HttpService,
    private activatedRoute: ActivatedRoute,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
   }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.cid = params['cid'];
      this.https.getSubcategory(this.cid).subscribe(
        success => {
          console.log(success);
          this.subcategories = success['data'];
        }, error => {
          alert('Network Error! Get Subcategory:' + error.statusText);
        });

        this.https.getCategorybyid(this.cid).subscribe(success => {
          this.category = success['data'][0];
        }, error => {
          alert('Network Error! Get Category by ID:' + error.statusText);
        })

        this.https.getCategory().subscribe(success => {
          console.log(success);
          this.categories = success['data'];
        }, error => {
          alert('Network Error! Get Category:' + error.statusText);
        });
    });
    setTimeout(function(){ $('.loader').fadeOut(); }, 2000);
  }
}
