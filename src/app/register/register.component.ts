import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { HttpService } from '../http.service';
import { SeoService } from '../seo.service';
import * as $ from 'jquery';
import { ToastrService } from 'ngx-toastr';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
} from "@angular/material";


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [
    HttpService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
  ]
})
export class RegisterComponent implements OnInit {
  send_data;
  nusername;
  userid;
  nemail;
  register = {
    "bid": "",
    "rid": "",
    "firstname": "",
    "lastname": "",
    "email": "",
    "password": "",
    "phone": ""
  }

  constructor(
    private router: Router,
    private https: HttpService,
    private _location: Location,
    private snackBar: MatSnackBar,
    private seoservice: SeoService,
    private toastr: ToastrService,
  ) {
    this.seoservice.addMeta('description', '');
    this.seoservice.addMeta('keyword', '');
  }

  submit() {
    this.send_data =
    {
      "bid": "1",
      "firstname": this.register.firstname,
      "lastname": this.register.lastname,
      "email": this.register.email,
      "password": this.register.password,
      "phone": this.register.phone
    }
    if (this.validate(this.send_data)) {
      this.https.createuser(this.send_data).subscribe(success => {
        console.log(success);
        if (success['status'] == 200) {
          sessionStorage.setItem('userid', success['data'][0].memberid);
          this.router.navigate(['/']);
        } else {
          alert(success['message']);
        }
      }, error => {
        alert('Network Error! Create Business Register:' + error.statusText);
      });
    }
  }



  validateEmail(email) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) {
      return false;
    }
    return true;
  }

  validate(send_data) {

    var succ = 1;

    if ((send_data.firstname == undefined) || (send_data.firstname == "")) {

      succ = 0;
      this.toastr.error('Enter a first name!', '', {
        timeOut: 6000
      });

    }

    if ((send_data.lastname == undefined) || (send_data.lastname == "")) {
      succ = 0;
      this.toastr.error('Enter a last name!', '', {
        timeOut: 6000
      });

    }

    if ((send_data.email == undefined) || (send_data.email == "")) {

      succ = 0;
      this.toastr.error('Enter a valid email address!', '', {
        timeOut: 6000
      });

    }

    if ((send_data.password == undefined) || (send_data.password == "")) {

      succ = 0;
      this.toastr.error('Enter a password!', '', {
        timeOut: 6000
      });
    }



    if (!this.validateEmail(send_data.email)) {
      succ = 0;
      this.toastr.error('Invalid Email Address!', '', {
        timeOut: 6000
      });

    }

    if (succ == 0)
      return false;
    else
      return true;

  }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);
  }

}
