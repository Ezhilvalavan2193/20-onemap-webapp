import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import { subscribeOn } from "rxjs/operator/subscribeOn";
import { SeoService } from '../seo.service';
import * as $ from 'jquery';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";

@Component({
  selector: 'app-enquiry',
  templateUrl: './enquiry.component.html',
  styleUrls: ['./enquiry.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBar,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule
  ]
})
export class EnquiryComponent implements OnInit {
  send_data;
    enquiry={
	    "email":"",
	    "name":"",
      "message":"",
      "phone":""  
}

  constructor(
    private https: HttpService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
   }


  Phonenumbervalid(value) {
    if(isNaN(value)){
      $("#register-btn").prop('disabled',true);
      $("#phone").val('');
     }else{
      $("#register-btn").removeAttr("disabled");
     }
  }
  
  sendenquiry() {
    this.send_data = this.enquiry;
    if (!this.validate(this.send_data)) {
    this.send_data = this.enquiry;
    this.https.enquiry(this.send_data).subscribe(success => {
      this.snackBar.open('Your enquiry submitted successfully!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Your enquiry submitted successfully!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);
      this.enquiry={
        "email":"",
        "name":"",
        "message":"",
        "phone":""  
  }
      return true;
    }, error => {
      alert('Network Error! Enquiry:' + error.statusText);
    });
  }
}

validateEmail(email){
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
  if (reg.test(email) == false) 
  {      
      return false;
  }
  return true;
}

  
  validate(send_data) {
    if ((send_data.name == undefined) || (send_data.name == "")) {
      this.snackBar.open('Name should not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Name should not Empty!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }
    
    if ((send_data.email == undefined) || (send_data.email == "")) {
      this.snackBar.open('email should not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('email should not Empty!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }

    

    if ((send_data.phone == undefined) || (send_data.phone == "")){
      this.snackBar.open('Phone should not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Phone should not Empty!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }

    if ((send_data.message == undefined) || (send_data.message == "")) {
      this.snackBar.open('Message should not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Message should not Empty!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }


    if(!this.validateEmail(send_data.email)){
      this.snackBar.open('Invalid Email Address!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      
      // $("#toast").html('Invalid Email Address!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);
      return true;
    }
    return false;
  }
   


  ngOnInit() {
  }

}
