import { Injectable } from '@angular/core';
import { BrowserModule, Meta } from '@angular/platform-browser';

@Injectable()
export class SeoService {

  author:string;
  desc:string;

  constructor(private meta: Meta) {}

  addMeta(name,desc) {
    this.meta.addTag({ name: name, content: desc });   
  }
}
