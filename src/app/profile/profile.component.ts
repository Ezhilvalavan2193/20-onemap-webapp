import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import * as _ from 'underscore';
import { PagerService } from '../pager.service';
import 'rxjs/add/operator/map';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import { GetnameService } from '../getname.service';
import { 
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatBadgeModule,
  MatExpansionModule
} from '@angular/material';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  providers: [
    HttpService, 
    PagerService, 
    SeoService, 
    GetnameService,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatBadgeModule,
    MatExpansionModule
  ]
})

export class ProfileComponent implements OnInit {
  view_data;
  category;
  userid;
  key = 'directorytitle';
  reverse = true;
  p: number = 1;
  filter:any;
  subcategory;
  subcategory_det:any = {};
  letter:any = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  pager: any = {};
  profile;
  profilelength;
  profiles;
  send_data;
  image_url = this.https.image_url;
  hasIcon;
  
  cities:any = [
    {'id':'0','name':'All'},{'id':'1','name':'Villupuram'},{'id':'2','name':'Tindivanam'},{'id':'3','name':'Panruti'}
  ];

  subcategorys;
  pagedItems = [];
  cid;
  sid;
  city;
  result;
  rate;

  pageIndex:number = 0;
  pageSize:number = 6;
  lowValue:number = 0;
  highValue:number = 6; 
  
  collection = [];
  subcategories;

  constructor(
    private activatedRoute: ActivatedRoute,
    private https: HttpService,
    private pagerService: PagerService,
    private seoservice: SeoService,
    private router: Router,
    private _getname: GetnameService,
  ) {
  }


  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
    this.activatedRoute.params.subscribe((params: Params) => {
      this.cid = 61;
      this.sid = params['sid'];
      this.city = params['cid'];
      this.result = params['result'];      
      if (this.city) {
        this.send_data = {
          'city': this.city,
          'subcategoryid':this.sid
        }

        this.https.getallprofilelist(this.send_data).subscribe(success => {
          this.profiles = [];
          this.profiles = success['data'];
          console.log(this.profiles)
          let i = 0;
          this.profiles.forEach(element => {
            this.profiles[i] = element;
            this.profiles[i].title = element.directorytitle.replace(/ /g, "-");
            this.profiles[i].title = this.profiles[i].title.replace(/_/g, "-");
            this.profiles[i].title = this.profiles[i].title.replace(/&amp;/g, "and");
            this.profiles[i].title = this.profiles[i].title.replace(/&/g, "and");
            this.profiles[i].title = this.profiles[i].title.replace(/--/g, "-");
            this.profiles[i].title = this.profiles[i].title.replace(/---/g, "-");
            this.profiles[i].title = this.profiles[i].title.replace(/"/g, "").replace(/'/g, "").replace(/\(|\)/g, "");
            i++;
          });
          this.setPage(1);
          this.profilelength = this.profiles.length;
        });
        this.category = "";
        this.subcategory = ""; 
      }
      this.cid = 61;
      this.https.getSubcategory(this.cid).subscribe(
        success => {
          this.subcategories = success['data'];
          
          let i = 0;
          this.subcategories.forEach(element => {
            this.subcategories[i] = element;
            this.subcategories[i].title = element.subcategory.replace(/ /g, "-");
            this.subcategories[i].title = this.subcategories[i].title.replace(/_/g, "-");
            this.subcategories[i].title = this.subcategories[i].title.replace(/&amp;/g, "and");
            this.subcategories[i].title = this.subcategories[i].title.replace(/&/g, "and");
            this.subcategories[i].title = this.subcategories[i].title.replace(/--/g, "-");
            this.subcategories[i].title = this.subcategories[i].title.replace(/---/g, "-");
            this.subcategories[i].title = this.subcategories[i].title.replace(/"/g, "").replace(/'/g, "").replace(/\(|\)/g, "");
            if(this.subcategories[i].subcategoryid == this.sid) {
              this.subcategory_det = this.subcategories[i];
              console.log(this.subcategory_det);
              this.seoservice.addMeta('description',this.subcategory_det.description);
              this.seoservice.addMeta('keyword',this.subcategory_det.keywords);
            }
            i++;
          });
        });
    });
    setTimeout(function () { $('.loader').fadeOut(); }, 2000);
  }

  findWord(i) {  
    window.scroll(0,0);  
    $(".word").removeClass('active');
    $(".activeword-"+i).addClass("active");  
    this.filter = this.letter[i];    
  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      window.scroll(0,0);
      return;
    }

    // get pager object from service
    this.pager = this.pagerService.getPager(this.profiles.length, page);

    // get current page of items
    this.pagedItems = this.profiles.slice(this.pager.startIndex, this.pager.endIndex + 1);
    window.scroll(0,0);
  }
  OpenMenu() {
    $(".cat-list").toggleClass('open');
    $(".cat-list").toggle();
  }

  clearFilter() {
    this.filter = "";
  }

  changeLocation(id) {
    let name = this.getCity(id);
    this.router.navigate(["/profiles",this.subcategory+'-'+name,this.sid,id]);
  }
  getCity(id) {
    if(id == '0') {
      return '';
    }
    if(id == '1') {
      return 'Villupuram';
    }
    if(id == '2') {
      return 'Tindivanam';
    }
    if(id == '3') {
      return 'Panruti';
    }
  }
  getPaginatorData(event){
    if(event.pageIndex === this.pageIndex + 1){
      this.lowValue = this.lowValue + this.pageSize;
      this.highValue =  this.highValue + this.pageSize;
     }
    else if(event.pageIndex === this.pageIndex - 1){
     this.lowValue = this.lowValue - this.pageSize;
     this.highValue =  this.highValue - this.pageSize;
    }   
   
     this.pageIndex = event.pageIndex;
  }
}
