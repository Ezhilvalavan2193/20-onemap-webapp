import { Injectable } from '@angular/core';

@Injectable()
export class GetnameService {

  constructor() { }

  getcategorytitle(id,items:any[]) {
    let name;
    items.forEach(element => {
      if(element.subcategoryid == id) {
        name = element.title;
      }
    });
    return name;
  }

  getcategoryname(id,items:any[]) {
    let name;
    items.forEach(element => {
      if(element.subcategoryid == id) {
        name = element.subcategory;
      }
    });
    return name;
  }

  getdirectoryname(id,items:any[]) {
    let name;
    items.forEach(element => {
      if(element.directoryid == id) {
        name = element.directorytitle;
      }
    });
    return name;
  }

  getcityname(id,items:any[]) {
    let name;
    items.forEach(element => {
      if(element.id == id) {
        name = element.name;
      }
    });
    return name;
  }

}
