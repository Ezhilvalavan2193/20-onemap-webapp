import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
} from "@angular/material";

@Component({
  selector: 'app-busregister',
  templateUrl: './busregister.component.html',
  styleUrls: ['./busregister.component.scss'],
  providers: [
    HttpService,
    SeoService,
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
  ]
})
export class BusregisterComponent implements OnInit {
  send_data;
  cart_method;
  userid; 
  busregister = {
  "bid":"",
  "rid":"",
  "username":"", 
  "email":"", 
  "password":"", 
  "phone":""
 }
  
  constructor(
    private http:Http,
    private activatedRoute: ActivatedRoute,
    private router:Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
  ) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
   }
  register(){
    this.send_data = 
    {       
      "bid":"1",
      "rid":"0",   
      "firstname":this.busregister.username, 
      "email":this.busregister.email, 
      "password":this.busregister.password, 
      "phone":this.busregister.phone
    } 
    if (!this.validate(this.send_data)) {
    this.https.createbusregister(this.send_data).subscribe(success => {
      console.log(success);
      if (success['status'] == 200) {        
        sessionStorage.setItem('userid', success['data'][0].memberid);
        this.router.navigate(['/businesslist']);
      } else {
        alert(success['message']);
      }
    }, error => {
      alert('Network Error! Create Business Register:' + error.statusText);
    });
  }
  }
  
  
  showhidepassword(){
    let classname;
    classname = $(".showhidepsw .fa").attr('class');

    if(classname == 'fa fa-eye') {

      $(".showhidepsw .fa").removeClass('fa-eye');
      $(".showhidepsw .fa").addClass('fa-eye-slash');
      $("#password").prop('type','password');
    } else {
      $(".showhidepsw .fa").removeClass('fa-eye-slash');
      $(".showhidepsw .fa").addClass('fa-eye');
      $("#password").prop('type','text');
    }

  }
  
  validateEmail(email){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) 
    {      
        return false;
    }
    return true;
  }


  Phonenumbervalid(value) {
    if(isNaN(value)){
      $("#register-btn").prop('disabled',true);
      $("#phone").val('');
     }else{
      $("#register-btn").removeAttr("disabled");
     }
  }

  validate(send_data) {

    if ((send_data.firstname == undefined) || (send_data.firstname == "")) {
      this.snackBar.open('Name should not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Name should not Empty!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }
    
    if ((send_data.email == undefined) || (send_data.email == "")) {
      this.snackBar.open('email should not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('email should not Empty!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }
    console.log(send_data);

    if ((send_data.password == undefined) || (send_data.password == "")) {
      this.snackBar.open('Password should not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Password should not Empty!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }

    if ((send_data.phone == undefined) || (send_data.phone == "")){
      this.snackBar.open('Phone should not Empty!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Phone should not Empty!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);

      return true;
    }
    if(!this.validateEmail(send_data.email)){
      this.snackBar.open('Invalid Email Address!', '' , {
        verticalPosition: 'top',
        horizontalPosition: 'end',
        duration: 5000,
        panelClass: ['custom-snackbar'],
      });
      // $("#toast").html('Invalid Email Address!');
      // $("#toast").addClass('show');
      // setInterval(function () {
      //   $("#toast").removeClass('show');
      // }, 5000);
      return true;
    }
    return false;
  }
   


  ngOnInit() {

    this.activatedRoute.params.subscribe((params: Params) => {
      console.log("params===="+JSON.stringify(params));
    });

    setTimeout(function(){ $('.loader').fadeOut(); }, 2000);
  }

}
