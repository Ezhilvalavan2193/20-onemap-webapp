import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BusregisterComponent } from './busregister.component';

describe('BusregisterComponent', () => {
  let component: BusregisterComponent;
  let fixture: ComponentFixture<BusregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BusregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
