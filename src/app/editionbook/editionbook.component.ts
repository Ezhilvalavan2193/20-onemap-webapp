import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule,
  MatDialogModule,
  MatBadgeModule,
  MatExpansionModule
} from "@angular/material";

@Component({
  selector: 'app-editionbook',
  templateUrl: './editionbook.component.html',
  styleUrls: ['./editionbook.component.scss'],
  providers: [
    MatInputModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatDialogModule,
    MatBadgeModule,
    MatExpansionModule,
    HttpService,
    SeoService,
  ]
})
export class EditionbookComponent implements OnInit {
  userid;
  constructor(
    private https: HttpService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private seoservice: SeoService,
  ) { }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
  }

}
