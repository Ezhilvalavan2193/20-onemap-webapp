import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditionbookComponent } from './editionbook.component';

describe('EditionbookComponent', () => {
  let component: EditionbookComponent;
  let fixture: ComponentFixture<EditionbookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditionbookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditionbookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
