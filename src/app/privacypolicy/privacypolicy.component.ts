import { Component, OnInit } from '@angular/core';
import { SeoService } from '../seo.service';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBarModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule
} from "@angular/material";

@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.component.html',
  styleUrls: ['./privacypolicy.component.scss'],
  providers:[SeoService,]
})
export class PrivacypolicyComponent implements OnInit {
  userid;
  constructor(private seoservice: SeoService) {
    this.seoservice.addMeta('description','');
    this.seoservice.addMeta('keyword','');
   }

  ngOnInit() {
    this.userid = sessionStorage.getItem('userid');
  }

}
