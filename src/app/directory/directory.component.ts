import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Injectable } from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../http.service';
import * as $ from 'jquery';
import { SeoService } from '../seo.service';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { LoginDialogComponent } from '../directory/login-dialog/login-dialog.component';
import { IsLoadingService } from '@service-work/is-loading';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {
  MatInputModule,
  MatButtonModule,
  MatFormFieldModule,
  MatSnackBar,
  MatSelectModule,
  MatAutocompleteModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatIconModule,
  MatTableModule
} from "@angular/material";
import { ReviewDialogComponent } from './review-dialog/review-dialog.component';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.scss']
})
export class DirectoryComponent implements OnInit {

  @ViewChild('videoPlayer') videoplayer: ElementRef;

  name: any;
  cover: any;
  photo: any;
  lat: any;
  lng: any;
  title: any;
  tagline: any;
  description: any;
  phone:any;
  video_url: any;
  profileid: any;
  directory: any;
  ratingdata: any;
  twitter_url: any;
  facebook_url: any;
  instagram_url: any;
  address: any;
  firstname: any;
  count = 0;
  userid:any;
  fiveStarRating = 0;
  fourStarRating = 0;
  threeStarRating = 0;
  twoStarRating = 0;
  oneStarRating = 0;
  fiveStarRatingPercent = "0%";
  fourStarRatingPercent = "0%";
  threeStarRatingPercent = "0%";
  twoStarRatingPercent = "0%";
  oneStarRatingPercent = "0%";
  averageRating = 0;
  average = 0.0;
  reviewitems: any;
  cards: any;
  cardcount=0;
  favCount=0;
  send_data:any;
  config: SwiperOptions = {
    pagination: { el: '.swiper-pagination', clickable: true },
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev'
    },
    spaceBetween: 30
  };
  public safeURL: SafeResourceUrl;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private seoservice: SeoService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private toastr: ToastrService,
    private isLoadingService: IsLoadingService,
    private _sanitizer: DomSanitizer
  ) {

    this.userid = sessionStorage.getItem('userid');

    this.route.params.subscribe((params: Params) => {
      this.profileid = params['id'];
      this.name = params['title'];
    });

    this.https.getDirectory(this.profileid, this.userid).subscribe(
      success => {
       console.log("directory====" + JSON.stringify(success));
       
        if (success['status'] == 200) {
          this.directory = success['data'][0];
          this.title = this.directory.title;
          this.tagline = this.directory.tagline;
          this.description = this.directory.description;
          if(this.directory.cover)
             this.cover = this.https.image_url + this.directory.cover;
          else 
            this.cover = '../assets/images/cover.jpg';

          if(this.directory.logo)
             this.photo = this.https.image_url + this.directory.logo;
          else 
             this.photo = '../assets/images/profile.jpg';

          this.lat = parseFloat(this.directory.latitude);
          this.lng = parseFloat(this.directory.longitude);
          this.phone = this.directory.phone;
          this.favCount = this.directory.favCount;
          this.address = this.directory.postcode + ", " + this.directory.address + ", " + this.directory.town;
          this.video_url = this.directory.video_url;
          this.safeURL = this._sanitizer.bypassSecurityTrustResourceUrl(this.directory.video_url.replace("watch?v=", "embed/"));
          this.twitter_url = (this.directory.company_twitter ? this.directory.company_twitter : "javascript::void(0)");
          this.facebook_url = (this.directory.company_facebook ? this.directory.company_facebook : "javascript::void(0)");
          this.instagram_url = (this.directory.company_instagram ? this.directory.company_instagram : "javascript::void(0)");
          this.firstname = this.directory.firstname;
          console.log("directory====" + JSON.stringify(this.directory));
        }

      }, error => {
        alert('Network Error! Get Profile Details:' + error.statusText);
      });

    this.https.getItemRating(this.profileid).subscribe(success => {

      if (success['status'] == 200) {
        this.ratingdata = success['data'];
        this.count = this.ratingdata.count;
        this.fiveStarRating = this.ratingdata.fiveStarRating;
        this.fourStarRating = this.ratingdata.fourStarRating;
        this.threeStarRating = this.ratingdata.threeStarRating;
        this.twoStarRating = this.ratingdata.twoStarRating;
        this.oneStarRating = this.ratingdata.oneStarRating;
        this.average = this.ratingdata.average;
        this.averageRating = this.ratingdata.averageRating;
        this.fiveStarRatingPercent = this.ratingdata.fiveStarRatingPercent;
        this.fourStarRatingPercent = this.ratingdata.fourStarRatingPercent;
        this.threeStarRatingPercent = this.ratingdata.threeStarRatingPercent;
        this.twoStarRatingPercent = this.ratingdata.twoStarRatingPercent;
        this.oneStarRatingPercent = this.ratingdata.oneStarRatingPercent;
      }


    });

    this.https.getAllItemRating(this.profileid).subscribe(success => {
      if (success['status'] == 200) {
        this.reviewitems = success['data'];
      } else {
        this.reviewitems = [];
      }
    })

    this.https.getAllCards(this.profileid).subscribe(success => {
      if (success['status'] == 200) {
        this.cards = success['data'];
        this.cardcount = this.cards.length;
      } else {
        this.cards = [];
      }
    })

    this.seoservice.addMeta('description', '');
    this.seoservice.addMeta('keyword', '');
  }

  ngOnInit() {

    
    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);

  }

  toggleVideo(event: any) {
    this.videoplayer.nativeElement.play();
  }

  openLoginDialog(): void {
    let dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '400px',
      data: { profileid: this.profileid, name: this.name }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openDialog(): void {

    if (!sessionStorage.getItem('userid')) {
        this.openLoginDialog();
    }else{
        this.openReviewDialog();
    }
   
  }

  addFav(): void{
    if (!sessionStorage.getItem('userid')) {
      this.openLoginDialog();
    }else{
        this.addwhistlist();
    }
  }

  addwhistlist(): void{
    $('.ajax-add').addClass('loading');

    this.send_data =
    {
      "memberid": sessionStorage.getItem('userid'),
      "directory_id" : this.profileid
    }

    this.https.addfavourite(this.send_data).subscribe(success => {
      console.log(success);
      if (success['status'] == 200) {     
        if(success['data'].message == "added")
        {
          this.favCount = 1;
          this.toastr.success('Added bookmark successfully!', '', {
            timeOut: 6000
          });
        } else{
          this.favCount = 0;
          this.toastr.success('Removed bookmark successfully!', '', {
            timeOut: 6000
          });
        }  

        $('.ajax-add').removeClass('loading');
      } 
    }, error => {
     // alert('Network Error! Create Business Register:' + error.statusText);
    });

  }

  openReviewDialog(): void {
    let dialogRef = this.dialog.open(ReviewDialogComponent, {
      width: '400px',
      data: { profileid: this.profileid, name: this.name }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  showPhone(): void{
    $('.phone-hide').addClass('show');
  }

  downloadClick(id): void{

    this.showloader();

    this.https.downloadPDF(id).subscribe(success => {
      console.log(success);
      if (success['status'] == 200) { 
        this.hideloader();
        this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          window.location.href = success['data'].pdf_url;
      });
      }
    });
  }

  showloader()
  {
    this.isLoadingService.add({key: ['default', 'single']});
    this.isLoadingService.add({key: 'button-one'});
  }

  hideloader()
  {
    this.isLoadingService.remove({key: ['default', 'single']});
    this.isLoadingService.remove({key: 'button-one'});
  }
}
