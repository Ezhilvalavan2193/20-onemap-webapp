import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpService } from '../../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import { LoginDialogComponent } from '../login-dialog/login-dialog.component';

@Component({
  selector: 'app-register-dialog',
  templateUrl: './register-dialog.component.html',
  styleUrls: ['./register-dialog.component.scss'],
  providers : [
    HttpService
  ]
})
export class RegisterDialogComponent implements OnInit {

  send_data:any;
  errorMsg = '';
  register = {
    "firstname": "",
    "lastname": "",
    "email": "",
    "password": "",
    "phone": ""
  }

  constructor(
    private https: HttpService,
    private router: Router,
    public dialogRef: MatDialogRef<RegisterDialogComponent>,
    public lgdialogRef : MatDialogRef<LoginDialogComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log("dta=="+JSON.stringify(data));
     }

  ngOnInit() {
  }

  submitClick(): void{
    this.send_data =
    {
      "bid": "0",
      "firstname": this.register.firstname,
      "email": this.register.email,
      "password": this.register.password,
    }
  if (this.validate(this.send_data)) {
    this.https.createuser(this.send_data).subscribe(success => {
      console.log(success);
      if (success['status'] == 200) {        
        this.dialog.closeAll();
        sessionStorage.setItem('userid', success['data'][0].memberid);
        sessionStorage.setItem('role', success['data'][0].status);
        let currentUrl = this.router.url;
            this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                this.router.navigate([currentUrl]);
            });

      } else {
        alert(success['message']);
      }
    }, error => {
      alert('Network Error! Create Business Register:' + error.statusText);
    });
  }
  }

  validate(send_data) {

    var succ = 1;
    if ((send_data.firstname == undefined) || (send_data.firstname == "")) {
      succ = 0;
    }

    if ((send_data.email == undefined) || (send_data.email == "")) {
     succ = 0;
    }

    if ((send_data.password == undefined) || (send_data.password == "")) {
     succ = 0;
    }
    
    if(!this.validateEmail(send_data.email)){
     succ = 0;
    }

    if(succ == 0)
      return false;
    else
      return true;
      
  }

  

  validateEmail(email){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(email) == false) 
    {      
        return false;
    }
    return true;
  }

  loginClick(): void{

    this.dialog.closeAll();

    let dialogRef = this.dialog.open(LoginDialogComponent, {
      width: '400px',
      data: { profileid : this.data.profileid, name : this.data.name}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });

  }

}
