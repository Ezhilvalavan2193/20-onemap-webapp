import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpService } from '../../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-review-dialog',
  templateUrl: './review-dialog.component.html',
  styleUrls: ['./review-dialog.component.scss'],
  providers: [
    HttpService
  ]
})
export class ReviewDialogComponent implements OnInit {

  send_data: any;
  review={
    "rating" : "",
    "title" : "",
    "comments" : ""
  };
  rate = '1';

  constructor(
    private https: HttpService,
    private router: Router,
    public dialogRef: MatDialogRef<ReviewDialogComponent>,
    public dialog: MatDialog,
    private _flashMessagesService: FlashMessagesService,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    console.log("dta==" + JSON.stringify(data));
  }

  ngOnInit() {
  }

  submitClick(): void{

    this.send_data = {
      "rating" : this.rate,
      "title" : this.review.title,
      "comments" : this.review.comments,
      "directoryid" : this.data.profileid
    }

    if(this.validate(this.send_data))
    {
        this.https.submitreview(sessionStorage.getItem('userid'), this.send_data).subscribe(success => {
          console.log(success);
          if (success['status'] == 200) {        
            this.dialog.closeAll();

            this._flashMessagesService.show('Thanks for your feedback!', { cssClass: 'alert alert-success', timeout: 6000 });

            let currentUrl = this.router.url;
                this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                    this.router.navigate([currentUrl]);
                });
    
          } else {
            alert(success['message']);
          }
        });
    }
   // alert(this.rate);
  }

  validate(send_data)
  {
     var succ = 1;

     if(send_data.title == undefined || send_data.title == "")
     {
       succ = 0;
     }

     if(send_data.comments == undefined || send_data == "")
     {
       succ = 0;
     }

     if(succ == 0)
        return false;
     else
        return true;
  }

}
