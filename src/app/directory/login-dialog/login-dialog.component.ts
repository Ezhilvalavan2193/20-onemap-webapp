import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpService } from '../../http.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog } from '@angular/material';
import { RegisterDialogComponent } from '../register-dialog/register-dialog.component';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss'],
  providers : [
    HttpService
  ]
})
export class LoginDialogComponent implements OnInit {

  send_data:any;
  errorMsg = '';
  login = {
    "email": "",
    "password": ""
  }

  constructor(
    private https: HttpService,
    private router: Router,
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    public regdialogRef: MatDialogRef<RegisterDialogComponent>,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log("dta=="+JSON.stringify(data));
     }

  ngOnInit() {
  }

  submitClick(): void {
    // this.dialogRef.close();
    this.send_data = {
      "email" : this.login.email,
      "password" : this.login.password
    }

    if(this.validate(this.send_data))
    {
        this.https.createLogin(this.send_data).subscribe(success => {
          console.log("success=="+JSON.stringify(success));
          if (success['status'] == 200) {  
            this.errorMsg = '';
            this.dialog.closeAll();
            sessionStorage.setItem('userid', success['data'][0].memberid);
            sessionStorage.setItem('role', success['data'][0].status);
            let currentUrl = this.router.url;
            this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
                this.router.navigate([currentUrl]);
            });
           
          }else{
            this.errorMsg = success['message'];
          }
        }, error => {

        });
    // console.log("submit dta=="+JSON.stringify(this.send_data));
    }

  }

  registerClick(): void{

    this.dialog.closeAll();

    let dialogRef = this.dialog.open(RegisterDialogComponent, {
      width: '400px',
      data: { profileid : this.data.profileid, name : this.data.name}
    });
  
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });

  }

  validate(send_data)
  {
     var succ = 1;

     if(send_data.email == undefined || send_data.email == "")
     {
       succ = 0;
     }

     if(send_data.password == undefined || send_data.password == "")
     {
       succ = 0;
     }

     if(succ == 0)
     return false;
    else
      return true;
  }

  hyphenateUrlParams(str:string){
    return str.replace(' ', '-');
  }


}
