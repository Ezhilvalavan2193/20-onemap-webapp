import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendinglistingComponent } from './pendinglisting.component';

describe('PendinglistingComponent', () => {
  let component: PendinglistingComponent;
  let fixture: ComponentFixture<PendinglistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendinglistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendinglistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
