import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouritelistingComponent } from './favouritelisting.component';

describe('FavouritelistingComponent', () => {
  let component: FavouritelistingComponent;
  let fixture: ComponentFixture<FavouritelistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouritelistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouritelistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
