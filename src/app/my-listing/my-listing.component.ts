import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpService } from '../http.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-my-listing',
  templateUrl: './my-listing.component.html',
  styleUrls: ['./my-listing.component.scss']
})
export class MyListingComponent implements OnInit {

  resultData: any;
  count = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private https: HttpService,
    private toastr: ToastrService
  ) {

    this.https.getAllDirectoryList(sessionStorage.getItem('userid')).subscribe(
      success => {
        if (success['status'] == 200) {
          this.resultData = success['data'];
          this.count = this.resultData.length;
        } else {
          this.resultData = [];
          this.count = 0;
        }
      });

  }

  ngOnInit() {

    if (!sessionStorage.getItem('userid')) {
      this.router.navigate(['/login']);
    }

    setTimeout(function () { $('#preloader').fadeOut(); }, 2000);
  }

  view(id): void {
    // alert(id);
    this.router.navigate(['/view', id]);

  }

  deleteClick(id): void {

    if (confirm("Are you sure you want to delete this listing?")) {
      this.https.deleteDirectory(id).subscribe(
        success => {
          console.log("succcc===" + JSON.stringify(success));
          if (success['status'] == 200) {
            this.toastr.success('Successfully deleted!', '', {
              timeOut: 6000
            });

            let currentUrl = this.router.url;
            this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
              this.router.navigate([currentUrl]);
            });
          } else {
            this.toastr.error('Sorry error in delete', '', {
              timeOut: 3000
            });
          }

        });
    }

    return;

  }

}
