import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlllistingComponent } from './alllisting.component';

describe('AlllistingComponent', () => {
  let component: AlllistingComponent;
  let fixture: ComponentFixture<AlllistingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlllistingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlllistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
